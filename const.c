#include "const.h"
#include "ctype.h"
#include "math.h"

void const_parse_number(struct const_value *restrict cv, const char *restrict str)
{
    const char *restrict p = str;
    long double place_value, exponent;
    cv->type = CBT_INT;
    cv->v.i = 0;
    if (*p == '0'){
        // octal or hexadecimal
        p++;
        if (tolower(*p) == 'x'){
            // hexadecimal
            p++;
            while (isdigit(*p) || (tolower(*p) >= 'a' && tolower(*p) <= 'f')){
                if (isdigit(*p)){
                    cv->v.i = (cv->v.i << 4) | (*p - '0');
                } else {
                    cv->v.i = (cv->v.i << 4) | ((tolower(*p) - 'a') + 10);
                }
                p++;
            }
        } else {
            // octal
            p++;
            while (*p >= '0' && *p <= '7'){
                cv->v.i = (cv->v.i << 3) | (*p - 0);
                p++;
            }
        }
    } else if (*p >= '0' && *p <= 9){
        // decimal
        while (*p >= '0' && *p <= '9'){
            cv->v.i = (cv->v.i * 10) + (*p - '0');
            p++;
        }
        if (*p == '.'){
            // decimal part
            cv->v.ld = cv->v.i;
            cv->type = CBT_LONGDOUBLE;
            place_value = 0.1;
            p++;
            while (*p >= '0' && *p <= '9'){
                cv->v.ld = (*p - '0') * place_value;
                place_value /= 10.0;
                p++;
            }
        }
        if (tolower(*p) == 'e'){
            // exponent part
            if (cv->type != CBT_LONGDOUBLE){
                cv->v.ld = cv->v.i;
                cv->type = CBT_LONGDOUBLE;
            }
            p++;
            exponent = 0.0;
            while (*p >= '0' && *p <= '9'){
                exponent = (exponent * 10.0) + (*p - '0');
                p++;
            }
            cv->v.ld *= powl(10, exponent);
        }
    }
}

