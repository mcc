#include "icode.h"
#include "errors.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"

static int block_id = 0;
static int op_id = 0;

static const char *icop_strs[] = {
    "nop",
    "add",
    "sub",
    "mul",
};

void icblock_create(struct icblock *bl)
{
    bl->o_first = bl->o_last = NULL;
    bl->id = ++block_id;
}

void icblock_destroy(struct icblock *bl)
{
    struct icop *op, *op_next;
    op = bl->o_first;
    while (op){
        op_next = op->next;
        free(op);
        op = op_next;
    }
}

static void icop_dump(struct icop *op)
{
    int i;
    printf("    %d %s", op->id, icop_strs[op->op]);
    for (i=0; i<2; ++i){
        if (op->operands[i]){
            printf("%s%d", (i==0)?" ":",", op->operands[i]->id);
        }
    }
    printf("\n");
}

void icblock_dump(struct icblock *bl)
{
    struct icop *op = bl->o_first;
    printf("block%d:\n", bl->id);
    while (op){
        icop_dump(op);
        op = op->next;
    }
}

static struct icop *icop_create(void)
{
    // TODO: create a pool of these?
    struct icop *op = emalloc(sizeof *op);
    memset(op, 0, sizeof *op);
    op->id = ++op_id;
    return op;
}

struct icop *icblock_append(struct icblock *bl)
{
    struct icop *op = icop_create();
    op->prev = bl->o_last;
    op->next = NULL;
    *(bl->o_last ? &bl->o_last->next : &bl->o_first) = op;
    bl->o_last = op;
    return op;
}
