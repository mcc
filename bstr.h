/*
 * better strings
 * - and better other things, for that matter!
 */

#ifndef _BSTR_H
#define _BSTR_H

#include "stdlib.h"
#include "stdarg.h"

// extended string functions
size_t strnlen(const char *str, size_t n);
char *strdup(const char *str);
char *strndup(const char *str, size_t n);
char *strchrnul(const char *str, int c);

// dynamically allocated string functions
char *strdcat(char **dest, const char *src);
char *strdncat(char **dest, const char *src, size_t n);
char *strdcpy(char **dest, const char *src);
char *strdncpy(char **dest, const char *src, size_t n);
char *strdcatc(char **dest, int ch);

// dynamically allocated string functions with explicit lengths
char *strldcat(char **dest, int *dest_len, const char *src);
char *strldncat(char **dest, int *dest_len, const char *src, size_t n);
char *strldcpy(char **dest, int *dest_len, const char *src, int src_len);
char *strldncpy(char **dest, int *dest_len, const char *src, size_t n);
char *strldcatc(char **dest, int *dest_len, int ch);

// dynamically allocated string formatting
char *sdprintf(const char *fmt, ...);
char *vsdprintf(const char *fmt, va_list ap);

// in-place case conversion
void strtolower(char *str);
void strtoupper(char *str);

// path functions
char *pathname(const char *str);
char *filesuffix(const char *str);

#endif
