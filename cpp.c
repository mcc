#include "cpp.h"
#include "errors.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "bstr.h"
#include "stdarg.h"
#include "stdbool.h"
#include "ctype.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "assert.h"

#define CONFIG_INCLUDE_DIRS "/usr/include"

//#define trace(...) fprintf(stderr, __VA_ARGS__)
#define trace(...) 

// A value from an expression
// (in #if etc.)
struct exprval {
    int value;
};

void cpp_delete_macro(struct cpp *cpp, struct macro *m);
void cpp_lex(struct cpp *cpp);

int cpp_get_col(struct cpp *cpp, char *p)
{
    return p + 1 - cpp->line_buf;
}

#define cpp_error(cpp, p, ...) cpp_message(cpp, p, NULL, "error: " __VA_ARGS__)
#define cpp_warning(cpp, p, ...) cpp_message(cpp, p, NULL, "warning: " __VA_ARGS__)
#define cpp_error_loc(cpp, sloc, ...) cpp_message(cpp, NULL, sloc, "error: " __VA_ARGS__)
#define cpp_warning_loc(cpp, sloc, ...) cpp_message(cpp, NULL, sloc, "warning: " __VA_ARGS__)
void cpp_message(struct cpp *cpp, char *p, struct sloc *sloc, const char *fmt, ...)
{
    va_list ap;
    struct sloc my_sloc;
    if (!sloc){
        my_sloc = cpp->line_loc;
        my_sloc.col = cpp_get_col(cpp, p);
        sloc = &my_sloc;
    }
    fprintf(stderr, "%s:%d: ",
        sloc->name,
        sloc->line/*,
        sloc->col*/);
    // NOTE: don't print col number, because after macro substitution,
    // the column number becomes incorrect. Fix=???
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fputc('\n', stderr);
}

void cpp_init(struct cpp *cpp)
{
    cpp->include_top = NULL;
    cpp->stale_files = NULL;
    cpp->line_buf = NULL;
    cpp->macros = NULL;
    cpp->include_dirs = estrdup(CONFIG_INCLUDE_DIRS);
}

void cpp_delete(struct cpp *cpp)
{
    struct incfile *incf, *incf_prev;
    incf = cpp->include_top;
    while (incf){
        incf_prev = incf->prev;
        free(incf);
        incf = incf_prev;
    }
    incf = cpp->stale_files;
    while (incf){
        incf_prev = incf->prev;
        free(incf);
        incf = incf_prev;
    }
    free(cpp->line_buf);
    free(cpp->include_dirs);
    while (cpp->macros){
        cpp_delete_macro(cpp, cpp->macros);
    }
}

struct macro *cpp_create_macro(const char *name)
{
    struct macro *m;
    m = emalloc(sizeof(struct macro) + strlen(name));
    m->next = NULL;
    m->args = NULL;
    m->has_args = false;
    m->text = NULL;
    strcpy(m->name, name);
    return m;
}

struct macro *cpp_create_push_macro(struct cpp *cpp, const char *name)
{
    struct macro *m;
    m = cpp_create_macro(name);
    m->next = cpp->macros;
    cpp->macros = m;
    return m;
}

struct macro *cpp_find_macro(struct cpp *cpp, const char *name)
{
    struct macro *m;
    m = cpp->macros;
    while (m && strcmp(m->name, name)){
        m = m->next;
    }
    return m;
}

struct macro *cpp_find_macro_arg(struct macro *m, const char *name)
{
    struct macro *m_arg;
    m_arg = m->args;
    while (m_arg && strcmp(m_arg->name, name)){
        m_arg = m_arg->next;
    }
    return m_arg;
}

static int macro_count_args(struct macro *m)
{
    struct macro *m_arg;
    int n;
    m_arg = m->args;
    n = 0;
    while (m_arg){
        n++;
        m_arg = m_arg->next;
    }
    return n;
}

void cpp_delete_macro(struct cpp *cpp, struct macro *m)
{
    struct macro *m_i, *m_arg, *m_arg_next;
    m_i = cpp->macros;
    if (m_i == m){
        cpp->macros = m->next;
    } else {
        while (m_i && m_i->next != m){
            m_i = m_i->next;
        }
        if (m_i){
            m_i->next = m->next;
        }
    }
    m_arg = m->args;
    while (m_arg){
        m_arg_next = m_arg->next;
        free(m_arg->text);
        free(m_arg);
        m_arg = m_arg_next;
    }
    free(m->text);
    free(m);
}


// push a file on the include stack
void cpp_include_file(struct cpp *cpp, const char *name, FILE *f, bool must_close)
{
    struct incfile *incf;
    incf = emalloc(sizeof(struct incfile) + strlen(name));
    incf->f = f;
    strcpy(incf->name, name);
    incf->line = 0;
    incf->must_close = must_close;
    incf->eof = false;
    incf->cond_d = incf->cond_td = 0;
    // push on include stack
    incf->prev = cpp->include_top;
    cpp->include_top = incf;
}

static bool get_dir(const char **dirlist, char **output)
{
    if (**dirlist){
        const char *p = strchrnul(*dirlist, ':');
        strdncpy(output, *dirlist, p - *dirlist);
        *dirlist = p;
        return true;
    } else {
        return false;
    }
}

// open a file and push it on the include stack
bool cpp_open_include_file(struct cpp *cpp, const char *name)
{
    FILE *f;
    const char *dirlist;
    char *attempt_file = NULL;
    struct stat stat_struct;

    // try current directory
    if (!stat(name, &stat_struct)){
        f = fopen(name, "r");
        assert(f != NULL);
        cpp_include_file(cpp, name, f, true);
        return true;
    }

    // try include dirs
    dirlist = cpp->include_dirs;
    while (get_dir(&dirlist, &attempt_file)){
        strdcatc(&attempt_file, '/');
        strdcat(&attempt_file, name);
        if (!stat(attempt_file, &stat_struct)){
            f = fopen(attempt_file, "r");
            assert(f != NULL);
            cpp_include_file(cpp, attempt_file, f, true);
            free(attempt_file);
            return true;
        }
    }
    free(attempt_file);
    return false;
}

void cpp_pop_include(struct cpp *cpp)
{
    struct incfile *incf;
    incf = cpp->include_top;
    if (incf){
        cpp->include_top = incf->prev;
        incf->prev = cpp->stale_files;
        cpp->stale_files = incf;
        if (incf->must_close == true){
            fclose(incf->f);
        }
    }
}

void cpp_clear_line(struct cpp *cpp)
{
    if (cpp->line_buf)
        strdcpy(&cpp->line_buf, "");
}

void cpp_delete_line(struct cpp *cpp)
{
    // TODO: can we, erm, keep the buffer?!
    free(cpp->line_buf);
    cpp->line_buf = NULL;
}

void cpp_read_line(struct cpp *cpp)
{
    struct incfile *incf;
    char **pstr = &cpp->line_buf;
    int ch;

    incf = cpp->include_top;
    if (incf && incf->eof){
        cpp_pop_include(cpp);
        incf = cpp->include_top;
    }
    if (incf){
        strdcpy(pstr, "");
        do {
            ch = fgetc(incf->f);
            if (ch == EOF){
                incf->eof = true;
            } else if (ch == '\\'){
                // backslashed newline?
                ch = fgetc(incf->f);
                if (ch == '\n'){
                    // yes
                    incf->line++;
                    strdcatc(pstr, ' '); // insert a space to ensure token separation
                    ch = '@'; // this could be anything
                } else {
                    // no
                    strdcatc(pstr, '\\');
                    strdcatc(pstr, ch);
                }
            } else if (ch != '\n'){
                strdcatc(pstr, ch);
            }
        } while (ch != EOF && ch != '\n');
        incf->line++;
        cpp->line_loc.name = incf->name;
        cpp->line_loc.line = incf->line;
    } else {
        cpp_delete_line(cpp);
    }
}

// read another line and stick it on the end of the buffer
void cpp_append_line(struct cpp *cpp)
{
    char *old_buf;
    old_buf = cpp->line_buf;
    cpp->line_buf = NULL;
    cpp_read_line(cpp);
    if (cpp->line_buf){
        strdcatc(&old_buf, ' ');
        strdcat(&old_buf, cpp->line_buf);
        free(cpp->line_buf);
    }
    cpp->line_buf = old_buf;
}

static void white(char **p, char *set)
{
    while (**p && strchr(set, **p)){
        (*p)++;
    }
}

// read a number
bool cpp_lex_number(struct cpp *cpp, char **p_start, char **output)
{
    char *p = *p_start;

    if (*p == '0'){
        // octal or hexadecimal
        p++;
        if (tolower(*p) == 'x'){
            // hexadecimal
            p++;
            while (isdigit(*p) || (tolower(*p) >= 'a' && tolower(*p) <= 'f')){
                p++;
            }
        } else {
            // octal
            p++;
            while (*p >= '0' && *p <= '7'){
                p++;
            }
        }
    } else if (*p >= '0' && *p <= '9'){
        // decimal
        while (*p >= '0' && *p <= '9'){
            p++;
        }
        if (*p == '.'){
            // decimal part
            p++;
            while (*p >= '0' && *p <= '9'){
                p++;
            }
        }
        if (tolower(*p) == 'e'){
            // exponent part
            p++;
            while (*p >= '0' && *p <= '9'){
                p++;
            }
        }
    } else {
        // not a number at all
        return false;
    }
    strdncat(output, *p_start, p - *p_start);
    *p_start = p;
    return true;
}

// read a string literal
bool cpp_lex_string(struct cpp *cpp, char *quotes, char **p_start, char **output)
{
    char *p = *p_start, quote;
    char *p_last_start;
    if (*p && strchr(quotes, *p)){
        quote = *p;
        p_last_start = *p_start;
        p++;
        if (output){
            strdcpy(output, "");
        }
        while (*p && *p != quote){
            if (*p == '\\'){
                if (p[1] == quote || p[1] == '\\'){
                    // escaped quotation mark or backslash
                    p += 2;
                    if (output){
                        strdncat(output, p_last_start, p - p_last_start);
                    }
                    p_last_start = p;
                } else {
                    p++;
                }
            } else {
                p++;
            }
        }
        if (*p != quote){
            cpp_error(cpp, *p_start, "unterminated %s literal",
                (quote == '\'') ? "character" : "string");
        } else {
            p++;
        }
        if (output){
            strdncat(output, p_last_start, p - p_last_start);
        }
        *p_start = p;
        return true;
    } else {
        return false;
    }
}

// read an identifier
bool cpp_lex_ident(struct cpp *cpp, char **p_start, char **output)
{
    char *p = *p_start;
    if (*p && (isalpha(*p) || *p == '_')){
        while (*p && (isalnum(*p) || *p == '_')){
            p++;
        }
        strdncpy(output, *p_start, p - *p_start);
        *p_start = p;
        return true;
    } else {
        return false;
    }
}

bool cpp_muted(struct cpp *cpp)
{
    struct incfile *incf;
    incf = cpp->include_top;
    if (incf){
        if (incf->cond_d > incf->cond_td){
            return true;
        }
    }
    return false;
}

void cpp_parse_include(struct cpp *cpp, char *p, char *p_start)
{
    char *inc_str = NULL, *p_str_start;
    white(&p, " \t");
    p_str_start = p;
    if (cpp_lex_string(cpp, "<\"", &p, &inc_str)){
        white(&p, " \t");
        if (*p){
            cpp_warning(cpp, p, "junk at end of #include directive");
        }
        if (strlen(inc_str) == 2){
            cpp_error(cpp, p_str_start, "empty filename in #include");
        } else {
            inc_str[strlen(inc_str) - 1] = '\0';
            if (!cpp_open_include_file(cpp, inc_str + 1)){
                cpp_error(cpp, p_str_start, "%s: no such file or directory", inc_str + 1);
            }
            free(inc_str);
        }
        cpp_clear_line(cpp);
    } else {
        cpp_error(cpp, p_start, "#include expects \"FILENAME\" or <FILENAME>");
    }
}

bool cpp_parse_macro_param(struct cpp *cpp, char **p, struct macro *m, struct macro ***p_next)
{
    char *param_name = NULL;
    struct sloc arg_loc;
    struct macro *m_arg;
    bool result;

    arg_loc = cpp->line_loc;
    arg_loc.col = cpp_get_col(cpp, *p);
    if (cpp_lex_ident(cpp, p, &param_name)){
        m_arg = cpp_find_macro_arg(m, param_name);
        if (m_arg){
            cpp_error_loc(cpp, &arg_loc, "repeated macro parameter \"%s\"", param_name);
        } else {
            m_arg = cpp_create_macro(param_name);
            **p_next = m_arg;
            *p_next = &m_arg->next;
        }
        result = true;
    } else {
        result = false;
    }
    free(param_name);
    return result;
}

void cpp_parse_define(struct cpp *cpp, char *p, char *p_start)
{
    char *macro_name = NULL;
    struct macro *m, **p_next;
    struct sloc name_sloc;
    bool failed = false;

    white(&p, " \t");
    name_sloc = cpp->line_loc;
    name_sloc.col = cpp_get_col(cpp, p);
    if (cpp_lex_ident(cpp, &p, &macro_name)){
        m = cpp_find_macro(cpp, macro_name);
        if (m){
            cpp_warning_loc(cpp, &name_sloc, "\"%s\" redefined", macro_name);
            cpp_warning_loc(cpp, &m->sloc, "previous definition was here");
            cpp_delete_macro(cpp, m);
        }
        m = cpp_create_push_macro(cpp, macro_name);
        free(macro_name);
        m->sloc = name_sloc;
        if (*p == '('){
            // macro has parameters
            m->has_args = true;
            p++;
            p_next = &m->args;
            white(&p, " \t");
            if (*p != ')'){
                do {
                    if (!strncmp(p, "...", 3)){
                        // variadic macro
                        struct macro *m_arg = cpp_create_macro("__VA_ARGS__");
                        *p_next = m_arg;
                        p_next = &m_arg->next;
                        p += 3;
                        white(&p, " \t");
                        break;
                    } else if (!cpp_parse_macro_param(cpp, &p, m, &p_next)){
                        failed = true;
                    }
                    white(&p, " \t");
                } while ((*p == ',') ? (
                    p++,
                    white(&p, " \t"),
                    true) : false);
            }
            *p_next = NULL;
            if (*p == ')'){
                p++;
            } else {
                cpp_error(cpp, p, "missing ')' in macro parameter list");
            }
        }
        white(&p, " \t");
        strdcpy(&m->text, p);
        cpp_clear_line(cpp);
        if (failed){
            cpp_delete_macro(cpp, m);
        }
    } else {
        cpp_error(cpp, p_start, "no macro name given in #define directive");
    }
}

void cpp_parse_undef(struct cpp *cpp, char *p, char *p_start)
{
    char *tok = NULL;
    struct macro *m;
    white(&p, " \t");
    if (cpp_lex_ident(cpp, &p, &tok)){
        white(&p, " \t");
        if (*p){
            cpp_warning(cpp, p, "junk at end of #undef directive");
        }
        m = cpp_find_macro(cpp, tok);
        if (m){
            cpp_delete_macro(cpp, m);
        }
        free(tok);
        cpp_clear_line(cpp);
    } else {
        cpp_error(cpp, p_start, "no macro name given in #undef directive");
    }
}

void cpp_do_condition(struct cpp *cpp, bool condition)
{
    if (!cpp_muted(cpp) && condition){
        cpp->include_top->cond_td++;
    }
    cpp->include_top->cond_d++;
    cpp_clear_line(cpp);
}

void cpp_parse_ifdef(struct cpp *cpp, char *p, char *p_start)
{
    char *tok = NULL;
    white(&p, " \t");
    if (cpp_lex_ident(cpp, &p, &tok)){
        white(&p, " \t");
        if (*p){
            cpp_warning(cpp, p, "junk at end of #ifdef directive");
        }
        cpp_do_condition(cpp, !!cpp_find_macro(cpp, tok));
        free(tok);
    } else {
        cpp_error(cpp, p_start, "no macro name given in #ifdef directive");
    }
}

// parse a C number - oct, hex or dec
static void parse_cnumber(const char *str, struct exprval *presult)
{
    unsigned long long result = 0;
    const char *p = str;
    if (*p == '0'){
        // octal or hexadecimal
        p++;
        if (tolower(*p) == 'x'){
            // hexadecimal
            p++;
            while (isdigit(*p) || (tolower(*p) >= 'a' && tolower(*p) <= 'f')){
                if (isdigit(*p)){
                    result = (result << 4) | (*p - '0');
                } else {
                    result = (result << 4) | ((tolower(*p) - 'a') + 10);
                }
                p++;
            }
        } else {
            // octal
            p++;
            while (*p >= '0' && *p <= '7'){
                result = (result << 3) | (*p - '0');
                p++;
            }
        }
    } else if (*p >= '0' && *p <= '9'){
        // decimal
        while (*p >= '0' && *p <= '9'){
            result = (result * 10) + (*p - '0');
            p++;
        }
        if (*p == '.'){
            // decimal part
            p++;
            while (*p >= '0' && *p <= '9'){
                // TODO: use value (floats, etc.)
                p++;
            }
        }
        if (tolower(*p) == 'e'){
            // exponent part
            p++;
            while (*p >= '0' && *p <= '9'){
                // TODO: use value (floats, etc.)
                p++;
            }
        }
    }
    presult->value = result;
}

void cpp_factor(struct cpp *cpp, char **p, struct exprval *result)
{
    char *factor_str = NULL;
    
    if(cpp_lex_number(cpp, p, &factor_str)){
        // it's a number
        parse_cnumber(factor_str, result);
        free(factor_str);
    } else if (**p == '!'){
        // logical not
        (*p)++;
        cpp_factor(cpp, p, result);
        result->value = !result->value;
    } else if (!strncmp(*p, "defined", 7)){
        // eg. #if defined(FOO)
        char *macro_name = NULL;
        struct macro *m;
        (*p) += 7;
        white(p, " \t");
        if (**p != '('){
            cpp_error(cpp, *p, "'(' expected");
        } else {
            (*p)++;
        }
        white(p, " \t");
        if (cpp_lex_ident(cpp, p, &macro_name)){
            m = cpp_find_macro(cpp, macro_name);
            result->value = !!m;
            free(macro_name);
        } else {
            cpp_error(cpp, *p, "identifier (macro name, for \"defined\") expected");
        }
        white(p, " \t");
        if (**p != ')'){
            cpp_error(cpp, *p, "')' expected");
        } else {
            (*p)++;
        }
    } else {
        cpp_error(cpp, *p, "expression expected");
    }
}

void cpp_and_expr(struct cpp *cpp, char **p, struct exprval *result)
{
    cpp_factor(cpp, p, result);
    white(p, " \t");
    while ((*p)[0] == '&' && (*p)[1] == '&'){
        struct exprval rhs;
        (*p) += 2;
        white(p, " \t");
        cpp_factor(cpp, p, &rhs);
        white(p, " \t");
        result->value = result->value && rhs.value;
    }
}

void cpp_or_expr(struct cpp *cpp, char **p, struct exprval *result)
{
    cpp_and_expr(cpp, p, result);
    white(p, " \t");
    while ((*p)[0] == '|' && (*p)[1] == '|'){
        struct exprval rhs;
        (*p) += 2;
        white(p, " \t");
        cpp_and_expr(cpp, p, &rhs);
        white(p, " \t");
        result->value = result->value || rhs.value;
    }
}

void cpp_expr(struct cpp *cpp, char **p, struct exprval *result)
{
    cpp_or_expr(cpp, p, result);
}

void cpp_parse_if(struct cpp *cpp, char *p, char *p_start)
{
    struct exprval result;
    white(&p, " \t");
    cpp_expr(cpp, &p, &result);
    cpp_do_condition(cpp, !!result.value);
}

void cpp_parse_ifndef(struct cpp *cpp, char *p, char *p_start)
{
    char *tok = NULL;
    white(&p, " \t");
    if (cpp_lex_ident(cpp, &p, &tok)){
        white(&p, " \t");
        if (*p){
            cpp_warning(cpp, p, "junk at end of #ifdef directive");
        }
        cpp_do_condition(cpp, !cpp_find_macro(cpp, tok));
        free(tok);
    } else {
        cpp_error(cpp, p_start, "no macro name given in #ifdef directive");
    }
}

void cpp_parse_else(struct cpp *cpp, char *p, char *p_start)
{
    white(&p, " \t");
    if (!cpp_muted(cpp) && *p){
        cpp_warning(cpp, p, "junk at end of #else directive");
    }
    if (cpp->include_top->cond_d == 0){
        cpp_error(cpp, p_start, "#else without matching #if or #ifdef");
    } else {
        if (cpp->include_top->cond_d == cpp->include_top->cond_td + 1){
            // false -> true
            cpp->include_top->cond_td++;
        } else if (!cpp_muted(cpp)){
            // true -> false
            cpp->include_top->cond_td--;
        }
    }
    cpp_delete_line(cpp);
}

void cpp_parse_endif(struct cpp *cpp, char *p, char *p_start)
{
    white(&p, " \t");
    if (!cpp_muted(cpp) && *p){
        cpp_warning(cpp, p, "junk at end of #endif directive");
    }
    if (cpp->include_top->cond_d == 0){
        cpp_error(cpp, p_start, "#endif without matching #if or #ifdef");
    } else {
        if (!cpp_muted(cpp)){
            cpp->include_top->cond_td--;
        }
        cpp->include_top->cond_d--;
    }
    if (cpp_muted(cpp))
        cpp_delete_line(cpp);
    else
        cpp_clear_line(cpp);
}

static struct {
    bool conditional_related;
    const char *str;
    void (* func)(struct cpp *cpp, char *p, char *p_start);
} directives[] = {
    {false, "define", cpp_parse_define},
    {false, "undef", cpp_parse_undef},
    {false, "include", cpp_parse_include},
    {true, "if", cpp_parse_if},
    {true, "ifdef", cpp_parse_ifdef},
    {true, "ifndef", cpp_parse_ifndef},
    {true, "else", cpp_parse_else},
    {true, "endif", cpp_parse_endif},
};

bool cpp_macro_args(struct cpp *cpp, char **line_buf, char **p, struct macro *m)
{
    const char *p_start;
    struct macro *m_arg;
    int n, depth, p_n, p_start_n;
    if (m->has_args){
        white(p, " \t");
        if (**p != '('){
            return false; // give up
        }
        (*p)++;

        // read arguments
        m_arg = m->args;
        n = 0;
        while (m_arg){
            white(p, " \t");
            p_start = *p;
            depth = 1;
            if (!strcmp(m_arg->name, "__VA_ARGS__")){
                // XXX: it's silly having this here - it's almost the same
                // as the one further down
                while (**p != '\0' && depth > 0){
                    if (cpp_lex_string(cpp, "\"'", p, NULL)){
                        // it's a string - let's not interpret '(' and ')' in strings!
                    } else if (**p == '('){
                        depth++;
                    } else if (**p == ')'){
                        depth--;
                    }
                    if (depth > 0){
                        (*p)++;
                    }
                }
            } else {
                for (;;){
                    while (**p != '\0' && depth > 0 && (**p != ',' || depth > 1)){
                        if (cpp_lex_string(cpp, "\"'", p, NULL)){
                            // it's a string - let's not interpret '(' and ')' in strings!
                        } else if (**p == '('){
                            depth++;
                        } else if (**p == ')'){
                            depth--;
                        }
                        if (depth > 0){
                            (*p)++;
                        }
                    }
                    if (**p == '\0' && cpp->include_top){
                        // there's a line break in the middle of it
                        p_n = *p - *line_buf;
                        p_start_n = p_start - *line_buf;
                        cpp_append_line(cpp);
                        *p = *line_buf + p_n;
                        p_start = *line_buf + p_start_n;
                    } else {
                        break;
                    }
                }
            }
            strdncpy(&m_arg->text, p_start, *p - p_start);
            //printf("arg=%s\n", m_arg->text);
            if (*p > p_start){
                n++;
            }
            if ((**p == ')' && m_arg->next)
            || (**p == ',' && !m_arg->next)){
                cpp_error(cpp, *p, "macro \"%s\" requires %d arguments, but only %d given", m->name, macro_count_args(m), n);
                break;
            } else if (**p == '\0'){
                if (cpp->include_top){
                } else {
                    cpp_error(cpp, *p, "unterminated argument list invoking macro \"%s\"", m->name);
                    break;
                }
            } else if (**p == ','){
                (*p)++;
            }
            m_arg = m_arg->next;
        }
        (*p)++;
        return true;
    } else {
        return true;
    }
}

// the spaghetti code is served
char *cpp_macro_arg_subst(struct cpp *cpp, struct macro *m)
{
    char *result = NULL, *s = NULL, *p, *p0, *p_before, *sp;
    struct macro *m_arg;
    int stringize = 0;

    p = m->text;
    p0 = p;
    while (*p){
        if (*p == '#'){
            if (p[1] == '#'){
                // token-paste
                if (p > p0){
                    strdncat(&result, p0, p - 1 - p0);
                }
                p += 2;
                while (*p == ' ' || *p == '\t'){
                    p++;
                }
                p0 = p;
                // remove whitespace after previous tok
                sp = result + strlen(result);
                while (sp > result && (sp[-1] == ' ' || sp[-1] == '\t')){
                    sp--;
                }
                *sp = '\0';
            } else {
                // stringize
                if (p > p0){
                    strdncat(&result, p0, p - p0);
                }
                stringize = 1;
                p++;
                p0 = p;
                while (*p == ' ' || *p == '\t'){
                    p++;
                }
                if (isalpha(*p) || *p == '_'){
                    goto identifier;
                } else {
                    cpp_error_loc(cpp, &m->sloc, "'#' not followed by macro-argument");
                    stringize = 0;
                }
            }
        } else if (isalpha(*p) || *p == '_'){
            // identifier - macro arg name?
        identifier:
            p_before = p;
            p++;
            while (isalnum(*p) || isdigit(*p) || *p == '_'){
                p++;
            }
            strdncpy(&s, p_before, p - p_before);
            m_arg = cpp_find_macro_arg(m, s);
            if (!m_arg){
                if (stringize){
                    cpp_error_loc(cpp, &m->sloc, "'#' not followed by macro-argument");
                    stringize = 0;
                }
                continue;
            }
            // macro-arg substitution
            if (p_before > p0){
                strdncat(&result, p0, p_before - p0);
            }
            strdcatc(&result, ' ');
            if (stringize){
                strdcatc(&result, '"');
            }
            if (m_arg->text)
                strdcat(&result, m_arg->text);
            if (stringize){
                strdcatc(&result, '"');
                stringize = 0;
            }
            p0 = p;
        } else {
            p++;
        }
    }
    if (stringize){
        cpp_error_loc(cpp, &m->sloc, "'#' not followed by macro-argument");
    }
    strdncat(&result, p0, p - p0);
    free(s);
    return result;
}

struct macro_stack {
    struct macro_stack *prev;
    struct macro *m;
};

void cpp_substitute_macros(struct cpp *cpp, char **line_buf, struct macro_stack *ms)
{
    char *p, *p_start, *ident = NULL;
    char *substituted_text, *new_line_buf = NULL;
    struct macro *m;
    struct macro_stack stack_item, *ms_ptr;
    int p_start_n, p_end_n;
    p = *line_buf;
    while (*p){
        p_start = p;
        if (cpp_lex_ident(cpp, &p, &ident)){
            // is it a macro?
            m = cpp_find_macro(cpp, ident);
            // is it in the stack?
            if (m){
                ms_ptr = ms;
                while (ms_ptr && ms_ptr->m != m){
                    ms_ptr = ms_ptr->prev;
                }
                if (ms_ptr){
                    m = NULL; // yes it is. Let's forget about it.
                }
            }
            p_start_n = p_start - *line_buf;
            if (m && cpp_macro_args(cpp, line_buf, &p, m)){
                p_start = *line_buf + p_start_n;
                substituted_text = cpp_macro_arg_subst(cpp, m);
                // now we have to substitute the substituted text
                stack_item.prev = ms;
                stack_item.m = m;
                cpp_substitute_macros(cpp, &substituted_text, &stack_item);
                strdncat(&new_line_buf, *line_buf, p_start - *line_buf);
                strdcat(&new_line_buf, substituted_text);
                free(substituted_text);
                p_end_n = strlen(new_line_buf); // de-pointerize p_start
                strdcat(&new_line_buf, p);
                // replace the line buffer with our new one (with the substitution)
                free(*line_buf);
                *line_buf = new_line_buf;
                new_line_buf = NULL;
                p = *line_buf + p_end_n;
            }
        } else if (cpp_lex_string(cpp, "\"'", &p, &ident)){
            // nothing needs to be done
            // but at least we've passed the string, so we
            // won't expand macros in it
        } else {
            p++;
        }
    }
    free(ident);
}

void cpp_remove_comments(struct cpp *cpp)
{
    char *p = cpp->line_buf, *p_start;
    int p_start_n, p_n;
    while (*p){
        if (p[0] == '/' && p[1] == '/'){
            // single-line comment
            memset(p, ' ', strlen(p));
            p += strlen(p);
        } else if (p[0] == '/' && p[1] == '*'){
            // multi-line comment
            p_start = p;
            p += 2;
            for (;;){
                if (p[0] == '*' && p[1] == '/'){
                    // end of comment
                    p += 2;
                    memset(p_start, ' ', p - p_start);
                    break;
                } else if (*p == '\0'){
                    // we need to read another line!
                    // as you can see, this isn't very elegant
                    p_start_n = p_start - cpp->line_buf;
                    p_n = p - cpp->line_buf;
                    cpp_append_line(cpp);
                    p_start = cpp->line_buf + p_start_n;
                    p = cpp->line_buf + p_n;
                } else {
                    p++;
                }
            }
        } else {
            p++;
        }
    }
}

void cpp_process_line(struct cpp *cpp)
{
    char *p, *tok = NULL, *p_start;
    int i;
    bool done;
    if (!cpp->line_buf)
        return;
    cpp_remove_comments(cpp);
    p = cpp->line_buf;
    white(&p, " \t");
    if (*p == '#'){
        // preprocessor directive
        p_start = p;
        p++;
        white(&p, " \t");
        done = false;
        cpp_lex_ident(cpp, &p, &tok);
        if (tok){
            for (i=0; i<(sizeof directives/sizeof *directives); i++){
                if (!strcmp(tok, directives[i].str)){
                    if (!cpp_muted(cpp) || directives[i].conditional_related){
                        directives[i].func(cpp, p, p_start);
                    }
                    done = true;
                }
            }
            if (!done && !cpp_muted(cpp)){
                cpp_error(cpp, p_start, "invalid preprocessing directive #%s", tok);
            }
        } else {
            cpp_clear_line(cpp);
        }
        free(tok);
    } else {
        if (!cpp_muted(cpp)){
            cpp_substitute_macros(cpp, &cpp->line_buf, NULL);
        }
    }
    if (cpp_muted(cpp)){
        cpp_delete_line(cpp);
    }
}
