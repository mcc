#ifndef MCC_CONST_H
#define MCC_CONST_H

#include "c_btypes.h"

// const basic types
#define CBT_INT 0
#define CBT_LONGLONG 1
#define CBT_LONGDOUBLE 2
#define CBT_STRING 3
#define CBT_UNSIGNED 0x10
#define CBT_SIGNED 0x20

struct const_value {
    int type;
    union {
        signed int i;
        unsigned int ui;
        signed long long ll;
        unsigned long long ull;
        long double ld;
        char *string;
    } v;
};

void const_parse_number(struct const_value *restrict cv, const char *restrict str);

#endif
