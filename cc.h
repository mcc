#ifndef MCC_CC_H
#define MCC_CC_H

struct cc;

#include "scanner.h"
#include "stree.h"

struct cc {
    struct lexer lex;
    struct stree *stree;
    struct stree *scope;
    struct stree *switch_scope; // innermost switch statement
};

#endif

