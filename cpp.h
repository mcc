#ifndef MCC_CPP_H
#define MCC_CPP_H

#include "stdio.h"
#include "stdbool.h"

// A location in a source file
struct sloc {
    const char *name;
    int line;
    int col;
};

// A source file
struct incfile {
    struct incfile *prev;
    FILE *f;
    int line;
    bool must_close; // must close f after use?
    bool eof;
    int cond_d, cond_td; // condition depth/condition true depth
    char name[1];
};

// A macro, or a macro argument
struct macro {
    struct macro *next;
    struct macro *args; // argument list
    bool has_args; // if has_args && !args then it's a macro like FOO()
    char *text; // what the macro expands to
    struct sloc sloc; // location of definition
    char name[1]; // macro/macro argument name
};

// Preprocessor state
struct cpp {
    struct incfile *include_top;
    struct incfile *stale_files;
    char *line_buf;
    struct sloc line_loc;
    struct macro *macros; // all defined macros
    char *include_dirs;
};

void cpp_init(struct cpp *cpp);
void cpp_delete(struct cpp *cpp);
bool cpp_open_include_file(struct cpp *cpp, const char *name);
void cpp_include_file(struct cpp *cpp, const char *name, FILE *f, bool must_close);
void cpp_read_line(struct cpp *cpp);
void cpp_process_line(struct cpp *cpp);

bool cpp_lex_number(struct cpp *cpp, char **p_start, char **output);

#endif

