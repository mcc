#ifndef MCC_STREE_H
#define MCC_STREE_H

#include "cc.h"
#include "stdio.h"

struct stree;

#include "const.h"
#include "c_btypes.h"
#include "symtab.h"

enum stree_form {
    // stree node 'form's
    STF_NONE = 0,
    STF_BINOP,
    STF_UNOP,
    STF_FACTOR,
    STF_TAG,        // tag (struct/union/enum)
    STF_ATOM,       // atom (identifier, eg. member name)
    STF_TYPE,       // type information, NOT a typedef
    STF_FUNCTION,
    STF_VARIABLE,   // variable, field or parameter
    STF_PARAMETERS, // function parameter list
    STF_BLOCK,      // compound statement
    STF_STAT,       // non-expression statement (if, while...)
    STF_PASS,       // nothing (placeholder)
    STF_LABEL,      // label or case label
    STF_JUMP,       // goto/break/continue/return
    // WARNING: Add strings to form_strtab in stree.c
    // when adding constants here!
};

// storage class
enum storage_class {
    STOR_NONE = 0,
    STOR_STATIC,
    STOR_EXTERN,
    STOR_COMMON,        // common storage class (ELF SHN_COMMON)
                        // - global variables declared but not defined
};

struct stree {
    struct stree *parent, *prev, *next, *child;
    int id; // unique id for debugging
    struct sloc sloc; // location of definition
    enum stree_form form;
    tok_t tok;
    struct btype btype;
    struct const_value cv;
    enum storage_class storage_class;
};

// used for recursion
struct stree_stack {
    struct stree *st_node;
    struct stree_stack *prev, *next;
};

struct stree *stree_create(void);
void stree_append_child(struct stree *st_parent, struct stree *st_child);
void stree_remove_child(struct stree *st_child);
void stree_destroy(struct stree *st); // destroy tree recursively
void stree_next_child(struct stree *st, struct stree **pchild); // set *pchild to next child, or NULL at end
void stree_dump(struct cc *cc, struct stree *st_root, FILE *output); // dump tree in a textual format
struct stree *stree_right(struct stree *st); // return st's last child
int stree_child_count(struct stree *st); // return number of children that st has

// symbol searching
struct stree *stree_find_local(struct stree *scope, enum stree_form form, tok_t tok);
struct stree *stree_get_child_by_form(struct stree *st, enum stree_form form);

#endif

