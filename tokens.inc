#ifndef MCC_TOKENS_H
#define MCC_TOKENS_H

#define TOK_IDENT 0x1000 // first identifier

#endif

#ifdef PUNCT

PUNCT(GE,       ">=")
PUNCT(LE,       "<=")
PUNCT(EQ,       "==")
PUNCT(NE,       "!=")
PUNCT(ARROW,    "->")
PUNCT(SHL,      "<<")
PUNCT(SHR,      ">>")
PUNCT(LAND,     "&&")
PUNCT(LOR,      "||")
PUNCT(INC,      "++")
PUNCT(DEC,      "--")
PUNCT(EADD,     "+=")
PUNCT(ESUB,     "-=")
PUNCT(EMUL,     "*=")
PUNCT(EDIV,     "/=")
PUNCT(EMOD,     "%=")
PUNCT(EAND,     "&=")
PUNCT(EOR,      "|=")
PUNCT(EXOR,     "^=")
PUNCT(ESHL,     "<<=")
PUNCT(ESHR,     ">>=")
PUNCT(ELLIPSIS, "...")
PUNCT(INVAL,    "<invalid-token>")
PUNCT(STR,      "<string-literal>")
PUNCT(CHARSTR,  "<character-literal>")
PUNCT(NUMBER,   "<numeric-literal>")
// these aren't really tokens, but it's convenient to treat them as such
PUNCT(POSTINC,  "<postfix++>")
PUNCT(POSTDEC,  "<postfix-->")
PUNCT(CAST,     "<cast>")

#endif

#ifdef DEF
DEF(__asm__)
DEF(auto)
DEF(break)
DEF(case)
DEF(char)
DEF(const)
DEF(continue)
DEF(define)
DEF(do)
DEF(double)
DEF(else)
DEF(endif)
DEF(enum)
DEF(extern)
DEF(float)
DEF(for)
DEF(goto)
DEF(if)
DEF(ifdef)
DEF(ifndef)
DEF(include)
DEF(int)
DEF(long)
DEF(register)
DEF(restrict)
DEF(return)
DEF(short)
DEF(signed)
DEF(sizeof)
DEF(static)
DEF(struct)
DEF(switch)
DEF(typedef)
DEF(union)
DEF(unsigned)
DEF(void)
DEF(volatile)
DEF(while)

#endif

