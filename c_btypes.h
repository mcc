#ifndef MCC_C_BTYPES_H
#define MCC_C_BTYPES_H

// a C type
// this structure can hold all the information
// for a basic type such as 'int', or 'const unsigned long'
// but for more complex types (structs, pointers), it can't.
enum btype_base_type {
    BT_VOID = 0,
    BT_CHAR,
    BT_INT,
    BT_FLOAT,
    BT_DOUBLE,
    BT_POINTER, // pointer to type in btype::node
    BT_STRUCT, // struct OR union (btype::node points to STF_TAG node)
};

// qualifiers and type-qualifiers
#define TQ_SHORT     (1 << 0)
#define TQ_LONG      (1 << 1)
#define TQ_LONG_LONG (1 << 2)
#define TQ_UNSIGNED  (1 << 3)
#define TQ_SIGNED    (1 << 4)
#define TQ_CONST     (1 << 5)
#define TQ_VOLATILE  (1 << 6)
#define TQ_RESTRICT  (1 << 7)

struct btype {
    enum btype_base_type base_type;
    int qualifiers; // TQ_* constants
    struct stree *node; // stree node for type
};

#endif

