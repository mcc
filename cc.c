#include "cc.h"
#include "stree.h"
#include "scanner.h"
#include "parse.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "errors.h"

int cpp_main(int argc, char **argv);
int cc_main(int argc, char **argv);

int cc_main(int argc, char **argv)
{
    struct cc cc_obj, *cc = &cc_obj;

    lex_create(&cc->lex);

    cpp_include_file(&cc->lex.cpp, "<stdin>", stdin, false);
    lex_start(&cc->lex);

    cc_parse(cc);

    stree_dump(cc, cc->stree, stdout);
    stree_destroy(cc->stree);
    lex_delete(&cc->lex);

    return 0;
}

int main(int argc, char **argv)
{
    if (strstr(argv[0], "cpp")){
        cpp_main(argc, argv);
    } else {
        cc_main(argc, argv);
    }
}
