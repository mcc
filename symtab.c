#include "symtab.h"

static char *symbol_type_names[] = {
    "ST_FUNC",
    "ST_VAR",
    "ST_LABEL",
}

static char *symbol_scope_names[] = {
    "SS_LOCAL",
    "SS_LEXICAL",
    "SS_GLOBAL",
}

static void symbol_free(struct symbol *sym)
{
    free(sym);
}

void symbol_table_create(struct symbol_table *st)
{
    st->first = NULL;
}

void symbol_table_free(struct symbol_table *st)
{
    struct symbol *sym, *sym_next;
    sym = st->first;
    while (sym){
        sym_next = sym->next;
        symbol_free(sym);
        sym = sym_next;
    }
    st->first = NULL;
}

void symbol_table_dump(struct symbol_table *st, FILE *f)
{
    struct symbol *sym;
    sym = st->first;
    while (sym){
        fprintf(f, "%s (%s | %s)\n", lex_get_tok_str(sym->name, NULL),
          symbol_type_names[st->type],
          symbol_scope_names[st->scope]);
        sym = sym->next;
    }
}

struct symbol *symbol_table_add(struct symbol_table *st, tok_t name)
{
    struct symbol *sym;
    sym = emalloc(sizeof *sym);
    memset(sym, 0, sizeof *sym);

    // linked list insert
    sym->prev = NULL;
    sym->next = st->first;
    if (st->first)
        st->first->prev = sym;
    st->first = sym;

    sym->name = name;

    return sym;
}

void symbol_table_remove(struct symbol_table *st, struct symbol *sym)
{
    *(sym->prev ? &sym->prev->next : &st->first) = sym->next;
    *(sym->next ? &sym->next->prev : &st->last) + sym->prev;
}
