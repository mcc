#include "parse.h"
#include "cc.h"
#include "scanner.h"
#include "const.h"
#include "errors.h"
#include "stdlib.h"
#include "stdio.h"
#include "stdarg.h"

// make-it-easier macros
#define LEX (&cc->lex) // struct lexer *
#define TOK (cc->lex.tok.tok) // tok_t

static void cc_accept(struct cc *cc, const char *description, ...);
static void cc_sync(struct cc *cc, ...);
static int get_precedence(tok_t tok);
static bool cc_isglobal(struct cc *cc);
static struct stree *cc_get_current_func(struct cc *cc);
static struct stree *cc_parse_factor(struct cc *cc);
static struct stree *cc_parse_expr(struct cc *cc);
static struct stree *cc_parse_expr_nocomma(struct cc *cc);
static bool cc_parse_struct(struct cc *cc, struct btype *struct_type_info);
static bool cc_parse_type(struct cc *cc, struct btype *type_info);
static struct stree *cc_parse_pointer_type(struct cc *cc, struct btype *type_info);
static struct stree *cc_parse_parameters(struct cc *cc);
static bool type_identical(struct btype *type1, struct btype *type2);
static void cc_compare_functions(struct cc *cc, struct stree *func1, struct stree *func2);
static void cc_parse_decl_function(struct cc *, struct stree **psym_node);
static void cc_parse_decl_variable(struct cc *, struct stree **psym_node);
static void cc_parse_statement(struct cc *cc);
static void cc_parse_compound_stat(struct cc *cc);
static void cc_parse_defn_function(struct cc *cc, struct stree *func);
static bool cc_parse_decl(struct cc *cc, struct stree *insert_here);

// throw a 'foo expected but bar found' error
void cc_expect(struct cc *cc, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    fprintf(stderr, "%s:%d: error: ", LEX->tok.tok_sloc.name, LEX->tok.tok_sloc.line);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, " expected but \"%s\" found\n", lex_get_tok_str(LEX, TOK, LEX->tok.tok_str));
}

// throw a compile error with explicit source code location (va_list form)
void cc_error_loc_v(struct cc *cc, struct sloc *sloc, const char *fmt, va_list ap)
{
    fprintf(stderr, "%s:%d: error: ", sloc->name, sloc->line);
    vfprintf(stderr, fmt, ap);
    fputc('\n', stderr);
}

// throw a compile error with explicit source code location (... form)
void cc_error_loc(struct cc *cc, struct sloc *sloc, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    cc_error_loc_v(cc, sloc, fmt, ap);
    va_end(ap);
}

// throw a compile error at current token
void cc_error(struct cc *cc, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    cc_error_loc_v(cc, &LEX->tok.tok_sloc, fmt, ap);
    va_end(ap);
}

// If the next token is in the given set of tokens,
// then accept it, otherwise throw an error.
// 'description' describes what is wanted. This could be
// a token or a non-terminal name (eg. "type" or "expression").
// Token set is a zero-terminated list of tokens. Eg.:
//   >  cc_accept(cc, "operator", '+', '-', TOK_SHL, 0);
// would throw "operator expected, but '*' found" if the next
// token wasn't either '+', '-' or TOK_SHL (<<)
static void cc_accept(struct cc *cc, const char *description, ...)
{
    va_list ap;
    tok_t tok;
    va_start(ap, description);
    while ((tok = va_arg(ap, int))){
        if (TOK == tok){
            // ok
            lex_next(LEX);
            return;
        }
    }
    cc_expect(cc, "%s", description);
}

// keep eating tokens until the current token is in the given set.
// The set is a zero-terminated list of tokens, eg.:
//   >  cc_sync(cc, ')', ';', 0);
static void cc_sync(struct cc *cc, ...)
{
    va_list ap;
    tok_t tok;
    for (;;){
        va_start(ap, cc);
        while ((tok = va_arg(ap, int))){
            if (TOK == tok)
                return;
        }
        va_end(ap);
        lex_next(LEX);
    }
}

static struct {
    int prec; // operator precedence level (1=highest)
    tok_t tok;  // operator token
} operator_table[] = {
    {1, '*'}, {1, '/'},    {1, '%'},                  // multiplicative
    {2, '+'}, {2, '-'},                            // additive
    {3, TOK_SHL}, {3, TOK_SHR},                    // shiftive?
    {4, '<'}, {4, TOK_LE}, {4, '>'}, {4, TOK_GE},  // comparison
    {5, TOK_EQ}, {5, TOK_NE},                      // equality
    {6, '&'},                                      // bitwise and
    {7, '^'},                                      // bitwise xor
    {8, '|'},                                      // bitwise or
    {9, TOK_LAND},                                 // logical and
    {10, TOK_LOR},                                 // logical or
    {11, '?'},                                     // ternary
    {12, '='}, {12, TOK_EADD}, {12, TOK_ESUB},     // augmented assignment
    {12, TOK_EMUL}, {12, TOK_EDIV}, {12, TOK_EMOD},// .
    {12, TOK_ESHL}, {12, TOK_ESHR}, {12, TOK_EAND},// .
    {12, TOK_EXOR}, {12, TOK_EOR},                 // .
    {13, ','},                                     // comma operator
};

static int get_precedence(tok_t tok)
{
    int i;
    for (i=0; i<(sizeof operator_table / sizeof *operator_table); i++){
        if (operator_table[i].tok == tok){
            return operator_table[i].prec;
        }
    }
    return 0;
}

// return true if parser state is in global scope
// (i.e. what is being passed is at global (file) scope)
static bool cc_isglobal(struct cc *cc)
{
    return (cc->scope == cc->stree);
}

// return the current function node
static struct stree *cc_get_current_func(struct cc *cc)
{
    // TODO!
    return NULL;
}

// parse a factor, except subexpressions (...),
// due to cast/subexpression ambiguity
static struct stree *cc_parse_factor(struct cc *cc)
{
    struct stree *fac = NULL;
    if (lex_is_ident(LEX, TOK)){
        fac = stree_create();
        fac->form = STF_FACTOR;
        fac->tok = TOK;
        lex_next(LEX);
    } else if (TOK == TOK_NUMBER){
        fac = stree_create();
        fac->form = STF_FACTOR;
        fac->tok = TOK_NUMBER;
        const_parse_number(&fac->cv, LEX->tok.tok_str);
        lex_next(LEX);
    } else if (TOK == TOK_STR){
        fac = stree_create();
        fac->form = STF_FACTOR;
        fac->tok = TOK_STR;
        fac->cv.type = CBT_STRING;
        fac->cv.v.string = LEX->tok.tok_str; // steal the string data :)
        LEX->tok.tok_str = NULL;
        lex_next(LEX);
    } else {
        cc_expect(cc, "expression");
    }
    return fac;
}

static struct stree *cc_parse_unary(struct cc *cc)
{
    struct stree *fac, *un, *prefix_first = NULL, *prefix_last = NULL;

    // prefix unary operators
    while (TOK == TOK_INC || TOK == TOK_DEC || TOK == '+'
      || TOK == '-' || TOK == '!' || TOK == '~' || TOK == '*'
      || TOK == '&' || TOK == TOK_sizeof){
        un = stree_create();
        un->form = STF_UNOP;
        un->tok = TOK;
        if (TOK == '+'){
            // The '+' does nothing. Does anyone ever use it?!
            fprintf(stderr, "You're using a unary plus? Weird.\n");
        }
link_prefix_unop:
        if (prefix_last){
            stree_append_child(prefix_last, un);
        } else {
            prefix_first = un;
        }
        prefix_last = un;
        lex_next(LEX);
    }

    // subexpression or cast
    // (a bit ambiguous)
    if (TOK == '('){
        struct stree *type_node;
        struct btype btype;
        lex_next(LEX);
        if (cc_parse_type(cc, &btype)){
            // cast
            type_node = cc_parse_pointer_type(cc, &btype);
            un = stree_create();
            un->form = STF_UNOP;
            un->tok = TOK_CAST;
            un->btype = btype;
            if (type_node){
                // excessive type information
                stree_append_child(un, type_node);
            }
            // I could choose between one goto, or reams and reams
            // of tangled spaghetti-loops.
            // I preferred the goto.
            // Sorry Dijkstra.
            goto link_prefix_unop;
        } else {
            fac = cc_parse_expr(cc);
            cc_accept(cc, "\")\"", ')', 0);
        }
    } else {
        fac = cc_parse_factor(cc);
    }

    // postfix unary operators
    while (TOK == TOK_INC || TOK == TOK_DEC || TOK == '[' || 
      TOK == TOK_ARROW || TOK == '.' || TOK == '('){
        un = stree_create();
        un->form = STF_UNOP;
        stree_append_child(un, fac);
        if (TOK == TOK_INC){
            un->tok = TOK_POSTINC;
            lex_next(LEX);
        } else if (TOK == TOK_DEC){
            un->tok = TOK_POSTDEC;
            lex_next(LEX);
        } else if (TOK == '['){
            // array subscript
            struct stree *subscript;
            un->tok = '[';
            lex_next(LEX);
            subscript = cc_parse_expr(cc);
            if (subscript){
                stree_append_child(un, subscript);
                cc_accept(cc, "\"]\"", ']', 0);
            }
        } else if (TOK == '.' || TOK == TOK_ARROW){
            // struct/union member access
            struct stree *member_name;
            un->tok = TOK;
            lex_next(LEX);
            if (lex_is_ident(LEX, TOK)){
                member_name = stree_create();
                member_name->form = STF_ATOM;
                member_name->tok = TOK;
                stree_append_child(un, member_name);
                lex_next(LEX);
            } else {
                cc_expect(cc, "identifier (member name)");
            }
        } else if (TOK == '('){
            struct stree *argument;
            // function call
            un->tok = '(';
            lex_next(LEX);
            if (TOK != ')'){
                do {
                    argument = cc_parse_expr_nocomma(cc);
                    if (argument){
                        stree_append_child(un, argument);
                    }
                } while (TOK == ',' ? (lex_next(LEX), 1) : 0);
            }
            cc_accept(cc, "\")\"", ')', 0);
        }
        fac = un;
    }
    
    if (prefix_last){
        stree_append_child(prefix_last, fac);
        return prefix_first;
    } else {
        return fac;
    }
}

static struct stree *cc_parse_expr_internal(struct cc *cc, int min_prec)
{
    struct stree *lhs, *rhs, *op_node, *node, *node_parent, *node_right;
    int prec, node_prec, op_tok;
    lhs = cc_parse_unary(cc);
    if (!lhs)
        return NULL;
    prec = get_precedence(TOK);
    while (prec != 0 && prec <= min_prec){
        op_tok = TOK;
        lex_next(LEX);
        rhs = cc_parse_unary(cc);
        if (!rhs){
            return lhs;
        }
        // find where to insert it
        node = lhs;
        node_parent = NULL;
        while (node && node->form == STF_BINOP){
            node_prec = get_precedence(node->tok);
            if (prec < node_prec){
                // carry on descending
                node_right = stree_right(node);
                if (node_right){
                    node_parent = node;
                    node = node_right;
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        if (node){
            // insert between node and node_parent
            op_node = stree_create();
            op_node->form = STF_BINOP;
            op_node->tok = op_tok;
            if (node_parent){
                stree_remove_child(node);
                stree_append_child(op_node, node);
                stree_append_child(op_node, rhs);
                stree_append_child(node_parent, op_node);
            } else {
                stree_append_child(op_node, node);
                stree_append_child(op_node, rhs);
                lhs = op_node;
            }
        } else {
            die("not sure");
        }
        prec = get_precedence(TOK);
    }
    return lhs;
}

static struct stree *cc_parse_expr(struct cc *cc)
{
    return cc_parse_expr_internal(cc, 13);
}

// same as cc_parse_expr, but don't parse the comma operator
// used in function calls
static struct stree *cc_parse_expr_nocomma(struct cc *cc)
{
    return cc_parse_expr_internal(cc, 12);
}

// parse struct or union (they're syntactically identical)
static bool cc_parse_struct(struct cc *cc, struct btype *struct_type_info)
{
    struct stree *tag;
    struct btype type_info, original_type_info;
    struct stree *type_node, *sym_node;
    tag = stree_create();
    tag->form = STF_TAG;
    tag->sloc = LEX->tok.tok_sloc;
    lex_next(LEX);
    if (lex_is_ident(LEX, TOK)){
        // prev_tag = stree_find(cc->scope, STF_TAG, TOK); TODO
        tag->tok = TOK;
        tag->sloc = LEX->tok.tok_sloc;
        lex_next(LEX);
    }
    if (TOK == '{'){
        // struct definition
        lex_next(LEX);
        while (TOK != 0 && TOK != '}'){
            if (cc_parse_type(cc, &original_type_info)){
                do {
                    type_info = original_type_info;
                    type_node = cc_parse_pointer_type(cc, &type_info);
                    if (!lex_is_ident(LEX, TOK)){
                        cc_expect(cc, "identifier");
                        if (type_node){
                            stree_destroy(type_node);
                        }
                        return false; // XXX: LEAKS?
                    }
                    sym_node = stree_create();
                    sym_node->form = STF_VARIABLE;
                    sym_node->btype = type_info;
                    sym_node->tok = TOK;
                    sym_node->sloc = LEX->tok.tok_sloc;
                    if (type_node){
                        stree_append_child(sym_node, type_node);
                    }
                    stree_append_child(tag, sym_node);
                    lex_next(LEX);
                    // TODO: check for duplicate fields
                } while (TOK == ',' ? (lex_next(LEX), true) : false);
            } else {
                // ???
                cc_expect(cc, "field or \"}\"");
            }
            cc_accept(cc, "\";\"", ';', 0);
        }
        cc_accept(cc, "\"}\"", '}', 0);
    } else {
    }
    stree_append_child(cc->scope, tag);
    struct_type_info->base_type = BT_STRUCT;
    struct_type_info->node = tag;
    return true;
}

static bool cc_parse_type(struct cc *cc, struct btype *type_info)
{
    type_info->base_type = 0;
    type_info->qualifiers = 0;
    switch (TOK){
        case TOK_void:
            type_info->base_type = BT_VOID; // well, it's a type, isn't it?
            // (HINT: the check for variables of type void is not done here)
            break;
        case TOK_char:
            type_info->base_type = BT_CHAR;
            break;
        case TOK_int:
            type_info->base_type = BT_INT;
            break;
        case TOK_float:
            type_info->base_type = BT_FLOAT;
            break;
        case TOK_double:
            type_info->base_type = BT_DOUBLE;
            break;
        case TOK_struct:
        case TOK_union:
            return cc_parse_struct(cc, type_info);
        default:
            return false;
    }
    lex_next(LEX);
    return true;
}

// Parse a pointer type: next token must be after initial type declaration
// i.e.:  int *foo;
//            ^--current token: '*'
// Returns a sub-stree of additional type information.
// THIS MUST BE PUT SOMEWHERE IN A TREE.
// *type_info will be modified to represent the new pointer type
static struct stree *cc_parse_pointer_type(struct cc *cc, struct btype *type_info)
{
    struct stree *type_node = NULL, *new_type_node;
    while (TOK == '*'){
        // create a type node to store the excess type information
        new_type_node = stree_create();
        new_type_node->form = STF_TYPE;
        new_type_node->btype = *type_info;
        if (type_node){
            stree_append_child(type_node, new_type_node);
        }
        type_node = new_type_node;
        // create pointer type
        type_info->base_type = BT_POINTER;
        type_info->qualifiers = 0; // TODO: parse qualifiers
        type_info->node = new_type_node;
        // eat '*'
        lex_next(LEX);
    }
    return type_node;
}

// Parse function parameters
// void foo(int a, int b);
//           ^-- current token: 'int'
// returns an STF_PARAMETERS node full of parameters, or
// NULL if there aren't any.
static struct stree *cc_parse_parameters(struct cc *cc)
{
    struct stree *parameters = NULL;
    if (TOK == TOK_void){
        // just 'void' - no parameters
        lex_next(LEX);
    } else if (TOK != ')'){
        parameters = stree_create(); // create parameter list
        parameters->form = STF_PARAMETERS;
        do {
            struct btype type_info;
            struct stree *type_node, *param_node;
            if (TOK == TOK_ELLIPSIS){
                // variadic function
                param_node = stree_create();
                param_node->form = STF_VARIABLE;
                param_node->tok = TOK_ELLIPSIS;
                if (stree_child_count(parameters) == 0){
                    cc_error(cc, "a named argument is required before \"...\"");
                }
                lex_next(LEX);
                if (TOK != ')'){
                    cc_error(cc, "variadic function's \"...\" must be placed at the end of the parameter list");
                    cc_sync(cc, ')', ';', 0);
                }
            } else {
                // parse parameter's type
                if (!cc_parse_type(cc, &type_info)){
                    cc_expect(cc, "type");
                }
                type_node = cc_parse_pointer_type(cc, &type_info);
                // create parameter node
                param_node = stree_create();
                param_node->form = STF_VARIABLE;
                param_node->btype = type_info;
                // save extra type info, if there is any
                if (type_node){
                    stree_append_child(param_node, type_node);
                }
                // does the parameter have a name yet? it may not.
                if (lex_is_ident(LEX, TOK)){
                    // parameter has a name
                    param_node->tok = TOK;
                    lex_next(LEX);
                }
                // if it doesn't, that's ok too
                // as long as it's not a function definition;
                // we don't know yet, though.
            }
            // Now, put the parameter in the list
            stree_append_child(parameters, param_node);
        } while (TOK == ',' ? (lex_next(LEX), true) : false);
    }
    cc_accept(cc, "\")\"", ')', 0);
    return parameters;
}

// return true if the two types are identical
static bool type_identical(struct btype *type1, struct btype *type2)
{
top:
    if (type1->base_type != type2->base_type)
        return false;
    // TODO: check qualifiers?
    if (type1->base_type == BT_POINTER){
        // compare pointed types
        type1 = &type1->node->btype;
        type2 = &type2->node->btype;
        goto top;
    }
    return true; // all checks passed
}

// compare two functions, func1 and func2, for compatibility
static void cc_compare_functions(struct cc *cc, struct stree *func1, struct stree *func2)
{
    struct stree *param_list_1, *param_list_2, *param_1, *param_2;
    int n;

    // check return types
    if (!type_identical(&func1->btype, &func2->btype)){
        cc_error_loc(cc, &func2->sloc, "conflicting types for \"%s\"",
          lex_get_tok_str(LEX, func2->tok, NULL));
        cc_error_loc(cc, &func1->sloc, "previous declaration of \"%s\" was here",
          lex_get_tok_str(LEX, func1->tok, NULL));
    }

    // check parameters
    param_list_1 = stree_get_child_by_form(func1, STF_PARAMETERS);
    param_list_2 = stree_get_child_by_form(func2, STF_PARAMETERS);
    if (stree_child_count(param_list_1) != stree_child_count(param_list_2)){
        // different number of parameters
        cc_error_loc(cc, &func2->sloc, "conflicting number of parameters for \"%s\"",
          lex_get_tok_str(LEX, func2->tok, NULL));
        cc_error_loc(cc, &func1->sloc, "previous declaration of \"%s\" was here",
          lex_get_tok_str(LEX, func1->tok, NULL));
    } else {
        // check each parameter
        param_1 = NULL;
        param_2 = NULL;
        n = 0;
        while (stree_next_child(param_list_1, &param_1),
          stree_next_child(param_list_2, &param_2),
          (param_1 && param_2)){
            n++;
            if (param_1->tok == TOK_ELLIPSIS || param_2->tok == TOK_ELLIPSIS){
                // variadic function
                if (param_1->tok == TOK_ELLIPSIS && param_2->tok == TOK_ELLIPSIS){
                    // that's ok
                } else {
                    cc_error_loc(cc, &func2->sloc, "\"%s\" is %svariadic, unlike previous declaration",
                      lex_get_tok_str(LEX, func2->tok, NULL),
                      param_1->tok == TOK_ELLIPSIS ? "not " : "");
                    cc_error_loc(cc, &func1->sloc, "previous declaration of \"%s\" was here",
                      lex_get_tok_str(LEX, func1->tok, NULL));
                }
            }
            // compare parameter types
            else if (!type_identical(&param_1->btype, &param_2->btype)){
                cc_error_loc(cc, &func2->sloc, "conflicting types for parameter %d of \"%s\"",
                  n, lex_get_tok_str(LEX, func2->tok, NULL));
                cc_error_loc(cc, &func1->sloc, "previous declaration of \"%s\" was here",
                  lex_get_tok_str(LEX, func1->tok, NULL));
            }
            // merge name information
            if (param_2->tok)
                param_1->tok = param_2->tok;
        }
    }
}

static void cc_parse_decl_function(struct cc *cc, struct stree **psym_node)
{
    struct stree *parameters, *prev_decl;
    struct stree *sym_node = *psym_node;

    if (!cc_isglobal(cc)){
        // Is a nested function
        struct stree *current_func;
        current_func = cc_get_current_func(cc);
        cc_error(cc, "nested functions are not permitted: \"%s\" inside \"%s\"",
          lex_get_tok_str(LEX, sym_node->tok, NULL),
          current_func ? lex_get_tok_str(LEX, current_func->tok, NULL) : "???");
    }

    // find previous declaration of this symbol
    prev_decl = stree_find_local(cc->scope, STF_FUNCTION, sym_node->tok);
    if (!prev_decl) prev_decl = stree_find_local(cc->scope, STF_VARIABLE, sym_node->tok);
    if (prev_decl){
        if (prev_decl->form == STF_FUNCTION){
            // must check that declarations match later
        } else {
            cc_error_loc(cc, &sym_node->sloc,
              "\"%s\" declared as different kind of symbol",
              lex_get_tok_str(LEX, sym_node->tok, NULL));
            cc_error_loc(cc, &prev_decl->sloc,
              "previous declaration of \"%s\" was here",
              lex_get_tok_str(LEX, sym_node->tok, NULL));
        }
    }
    sym_node->form = STF_FUNCTION;
    // parse parameters
    lex_next(LEX); // eat '('
    parameters = cc_parse_parameters(cc);
    if (parameters){
        stree_append_child(sym_node, parameters);
    }
    if (prev_decl && prev_decl->form == STF_FUNCTION){
        // make sure declarations match
        cc_compare_functions(cc, prev_decl, sym_node);
        // either way, we don't want two copies, so delete the later one
        stree_destroy(sym_node);
        *psym_node = NULL;
    }
}

static void cc_parse_decl_variable(struct cc *cc, struct stree **psym_node)
{
    struct stree *sym_node = *psym_node;
    struct stree *prev_defn;

    if (sym_node->btype.base_type == BT_VOID){
        cc_error_loc(cc, &sym_node->sloc, "cannot declare variable \"%s\" of type void: impossible without vacuum pump",
        lex_get_tok_str(LEX, sym_node->tok, NULL));
    }

    // find previous declaration of this symbol
    prev_defn = stree_find_local(cc->scope, STF_VARIABLE, sym_node->tok);
    if (prev_defn){
        if (prev_defn->storage_class == STOR_EXTERN
          || prev_defn->storage_class == STOR_COMMON){
            // that's OK
        } else {
            cc_error_loc(cc, &sym_node->sloc, "redefinition of \"%s\"",
              lex_get_tok_str(LEX, sym_node->tok, NULL));
            cc_error_loc(cc, &prev_defn->sloc,
              "previous definition of \"%s\" was here",
              lex_get_tok_str(LEX, sym_node->tok, NULL));
        }

        if (prev_defn->storage_class == STOR_EXTERN
          && sym_node->storage_class != STOR_EXTERN){
            prev_defn->storage_class = 0;
        }
        stree_destroy(sym_node);
        sym_node = prev_defn;
        *psym_node = NULL; // XXX: is this correct?
    }

    sym_node->form = STF_VARIABLE;
}

static void cc_parse_if_while_stat(struct cc *cc)
{
    struct stree *stat, *expr, *old_scope;
    stat = stree_create();
    stat->form = STF_STAT;
    stat->tok = TOK;
    stree_append_child(cc->scope, stat);
    lex_next(LEX);
    cc_accept(cc, "\"(\"", '(', 0);
    expr = cc_parse_expr(cc);
    cc_accept(cc, "\")\"", ')', 0);
    stree_append_child(stat, expr);

    old_scope = cc->scope;
    cc->scope = stat;
    cc_parse_statement(cc);
    cc->scope = old_scope;
}

static void cc_parse_do_stat(struct cc *cc)
{
    struct stree *stat, *expr, *old_scope;
    stat = stree_create();
    stat->form = STF_STAT;
    stat->tok = TOK;
    stree_append_child(cc->scope, stat);
    lex_next(LEX);

    old_scope = cc->scope;
    cc->scope = stat;
    cc_parse_statement(cc);
    cc->scope = old_scope;

    cc_accept(cc, "\"while\"", TOK_while, 0);
    cc_accept(cc, "\"(\"", '(', 0);
    expr = cc_parse_expr(cc);
    cc_accept(cc, "\")\"", ')', 0);
    stree_append_child(stat, expr);
}

static void cc_parse_for_stat(struct cc *cc)
{
    struct stree *stat, *expr, *old_scope;
    int i;

    stat = stree_create();
    stat->form = STF_STAT;
    stat->tok = TOK;
    stree_append_child(cc->scope, stat);
    lex_next(LEX);
    cc_accept(cc, "\"(\"", '(', 0);

    for (i=0; i<3; i++){
        if (TOK == (i == 2 ? ')' : ';')){
            expr = stree_create();
            expr->form = STF_PASS;
        } else {
            expr = cc_parse_expr(cc);
        }
        stree_append_child(stat, expr);
        cc_accept(cc,
            i == 2 ? "\")\"" : "\";\"",
            i == 2 ? ')' : ';',
            0);
    }

    old_scope = cc->scope;
    cc->scope = stat;
    cc_parse_statement(cc);
    cc->scope = old_scope;
}

static void cc_parse_switch_stat(struct cc *cc)
{
    struct stree *stat, *expr, *old_scope;
    stat = stree_create();
    stat->form = STF_STAT;
    stat->tok = TOK;
    stree_append_child(cc->scope, stat);
    lex_next(LEX);
    cc_accept(cc, "\"(\"", '(', 0);
    expr = cc_parse_expr(cc);
    cc_accept(cc, "\")\"", ')', 0);
    stree_append_child(stat, expr);

    old_scope = cc->scope;
    cc->scope = stat;
    cc_parse_statement(cc);
    cc->scope = old_scope;
}

static void cc_parse_statement(struct cc *cc)
{
    struct stree *expr, *block, *old_scope, *stat, *label;
top:
    if (TOK == TOK_if || TOK == TOK_while){
        cc_parse_if_while_stat(cc);
    } else if (TOK == TOK_do){
        cc_parse_do_stat(cc);
        cc_accept(cc, "\";\"", ';', 0);
    } else if (TOK == TOK_for){
        cc_parse_for_stat(cc);
    } else if (TOK == TOK_switch){
        cc_parse_switch_stat(cc);
    } else if (TOK == TOK_case){
        if (cc->switch_scope){
            label = stree_create();
            label->form = STF_LABEL;
            label->tok = TOK;
            stree_append_child(cc->scope, label);
            lex_next(LEX);
            expr = cc_parse_expr(cc);
            stree_append_child(label, expr);
            cc_accept(cc, "\":\"", ':', 0);
        } else {
            cc_error(cc, "case label not within a switch statement");
            cc_sync(cc, ':', ';', 0);
            lex_next(LEX);
        }
        goto top;
    } else if (TOK == TOK_continue || TOK == TOK_break){
        stat = stree_create();
        stat->form = STF_JUMP;
        stat->tok = TOK;
        stree_append_child(cc->scope, stat);
        lex_next(LEX);
        cc_accept(cc, "\"}\"", '}', 0);
    } else if (TOK == TOK_return){
        stat = stree_create();
        stat->form = STF_JUMP;
        stat->tok = TOK;
        stree_append_child(cc->scope, stat);
        lex_next(LEX);
        if (TOK != ';'){
            expr = cc_parse_expr(cc);
            stree_append_child(stat, expr);
        }
        cc_accept(cc, "\"}\"", '}', 0);
    } else if (TOK == TOK_goto){
        lex_next(LEX);
        if (lex_is_ident(LEX, TOK)){
            stat = stree_create();
            stat->form = STF_JUMP;
            stat->tok = TOK;
            stree_append_child(cc->scope, stat);
            lex_next(LEX);
            cc_accept(cc, "\";\"", ';', 0);
        } else {
            cc_expect(cc, "label");
        }
    } else if (TOK == '{'){
        lex_next(LEX);
        block = stree_create();
        block->form = STF_BLOCK;
        stree_append_child(cc->scope, block);
        old_scope = cc->scope;
        cc->scope = block;
        cc_parse_compound_stat(cc);
        cc->scope = old_scope;
        cc_accept(cc, "\"}\"", '}', 0);
    } else if (TOK == ';'){
        // null statement
        stat = stree_create();
        stat->form = STF_PASS;
        stree_append_child(cc->scope, stat);
        lex_next(LEX);
    } else {
        // is it a label?
        if (lex_is_ident(LEX, TOK)){
            struct token ident_token;
            token_dup(&LEX->tok, &ident_token);
            lex_next(LEX);
            if (TOK == ':'){
                // label
                label = stree_create();
                label->form = STF_LABEL;
                label->tok = ident_token.tok;
                stree_append_child(cc->scope, label);
                lex_next(LEX);
                token_free(&ident_token);
                goto top;
            } else {
                // wasn't a label
                lex_unget_tok(LEX, &ident_token);
            }
        }
        expr = cc_parse_expr(cc);
        if (expr){
            stree_append_child(cc->scope, expr);
            cc_accept(cc, "\";\"", ';', 0);
        }
    }
}

static void cc_parse_compound_stat(struct cc *cc)
{
    while (TOK != 0 && TOK != '}'){
        if (cc_parse_decl(cc, cc->scope)){

        } else {
            cc_parse_statement(cc);
        }
    }
}

static void cc_parse_defn_function(struct cc *cc, struct stree *func)
{
    struct stree *old_scope, *block;
    // TODO: check presence of parameter names
    // TODO: parse body
    block = stree_create();
    block->sloc = LEX->tok.tok_sloc;
    block->form = STF_BLOCK;
    stree_append_child(func, block);
    old_scope = cc->scope;
    cc->scope = block;
    cc_parse_compound_stat(cc);
    cc->scope = old_scope;
}

// Parse a declaration/definition.
// Returns true if it found one, false if there's no declaration (no tokens will have been eaten).
// Will return true on syntax error, since tokens were eaten.
static bool cc_parse_decl(struct cc *cc, struct stree *insert_here)
{
    struct btype type_info, original_type_info;
    struct stree *type_node, *sym_node = NULL;
    bool was_func_defn = false;
    enum storage_class storage_class = 0;

    if (TOK == TOK_extern || TOK == TOK_static){
        if (TOK == TOK_extern){
            storage_class = STOR_EXTERN;
        } else {
            storage_class = STOR_STATIC;
        }
        lex_next(LEX);
    }

    if (cc_parse_type(cc, &original_type_info)){
        do {
            // restore the type before the '*'s (pointer types)
            type_info = original_type_info;
            type_node = cc_parse_pointer_type(cc, &type_info);
            if (!lex_is_ident(LEX, TOK)){
                cc_expect(cc, "identifier");
                if (type_node){
                    stree_destroy(type_node);
                }
                return true;
            }

            // variable declaration or definition
            // or function declaration or definition
            sym_node = stree_create();
            sym_node->btype = type_info;
            sym_node->storage_class = storage_class;
            sym_node->tok = TOK;
            sym_node->sloc = LEX->tok.tok_sloc;
            lex_next(LEX);
            // put the type info somewhere
            if (type_node){
                stree_append_child(sym_node, type_node);
            }
            if (TOK == '('){
                // it is a function
                cc_parse_decl_function(cc, &sym_node);
                if (TOK == '{'){
                    // function definition
                    lex_next(LEX);
                    cc_parse_defn_function(cc, sym_node);
                    cc_accept(cc, "\"}\"", '}', 0);
                    was_func_defn = true;
                }
            } else {
                // it is a variable
                cc_parse_decl_variable(cc, &sym_node);
            }
            if (sym_node){
                stree_append_child(insert_here, sym_node);
            }
        } while (TOK == ',' ? (lex_next(LEX), true) : false);
        if (!was_func_defn){
            cc_accept(cc, "\";\"", ';', 0);
        }
        return true;
    } else if (storage_class){
        cc_expect(cc, "declaration");
        return true;
    } else {
        // hmm.
        return false;
    }
}

void cc_parse(struct cc *cc)
{
    cc->stree = cc->scope = stree_create();
    while (TOK != 0){
        if (!cc_parse_decl(cc, cc->scope)){
            break;
        }
    }
}
