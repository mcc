#include "scanner.h"
#include "errors.h"
#include "cpp.h"
#include "ctype.h"
#include "bstr.h"
#include "string.h"
#include "assert.h"

static char *keywords[] = {
#define DEF(x) #x,
#include "tokens.inc"
#undef DEF
};

static char *punctuation[] = {
#define PUNCT(x, str) str,
#include "tokens.inc"
#undef PUNCT
};

void token_dup(struct token *src, struct token *dest)
{
    *dest = *src;
    if (dest->tok_str){
        dest->tok_str = estrdup(src->tok_str);
    }
}

void token_free(struct token *token)
{
    free(token->tok_str);
}

void lex_error(struct lexer *lex, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    fprintf(stderr, "%s:%d: error: ", lex->tok.tok_sloc.name, lex->tok.tok_sloc.line);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fputc('\n', stderr);
}

// return token number for a keyword
static int find_keyword(const char *str)
{
    // binary search
    int l, u, try;
    char *try_str;
    int compare;
    l = TOK_FIRSTK + 1;
    u = TOK_LASTK - 1;
    do {
        try = (l + u) / 2;
        try_str = keywords[try - (TOK_FIRSTK + 1)];
        compare = strcmp(str, try_str);
        if (compare == 0){
            return try;
        } else if (compare < 0){
            u = try - 1;
        } else if (compare > 0){
            l = try + 1;
        }
    } while (l <= u);
    return 0;
}

void lex_create(struct lexer *lex)
{
    lex->pch = NULL;

    lex->tok.tok = 0;
    lex->tok.tok_str = NULL;
    lex->tok.tok_str_len = 0;

    lex->next_tok.tok = 0;
    lex->next_tok.tok_str = NULL;
    lex->next_tok.tok_str_len = 0;

    lex->next_ident_tok = TOK_IDENT;
    memset(lex->ident_hashtab, 0, IDENT_HASH_SIZE * sizeof(struct ident *));
    cpp_init(&lex->cpp);
}

void lex_delete(struct lexer *lex)
{
    int i;
    struct ident *id, *id_prev;
    for (i=0; i<IDENT_HASH_SIZE; i++){
        id = lex->ident_hashtab[i];
        while (id){
            id_prev = id->hash_prev;
            free(id);
            id = id_prev;
        }
    }
    free(lex->tok.tok_str);
    free(lex->next_tok.tok_str);
    cpp_delete(&lex->cpp);
}

// generate a (fairly simple) hash for a string
static int hash_str(const char *str, int hash_size)
{
    int hash_value = 0;
    while (*str){
        hash_value *= *str;
        str++;
    }
    return hash_value % hash_size;
}

struct ident *lex_get_ident_hashed(struct lexer *lex, const char *str, int hash)
{
    struct ident *ident;
    ident = lex->ident_hashtab[hash];
    while (ident && strcmp(ident->str, str)){
        ident = ident->hash_prev;
    }
    return ident;
}

// get, or create, a 'struct ident'
struct ident *lex_get_ident(struct lexer *lex, const char *str)
{
    int hash = hash_str(str, IDENT_HASH_SIZE);
    struct ident *ident = lex_get_ident_hashed(lex, str, hash);
    if (!ident){
        // create a new one
        ident = emalloc(sizeof(struct ident) + strlen(str));
        ident->hash_prev = lex->ident_hashtab[hash];
        lex->ident_hashtab[hash] = ident;
        ident->tok = lex->next_ident_tok++;
        strcpy(ident->str, str);
    }
    return ident;
}

// get a 'struct ident', but don't create it
struct ident *lex_get_ident_nocreate(struct lexer *lex, const char *str)
{
    int hash = hash_str(str, IDENT_HASH_SIZE);
    return lex_get_ident_hashed(lex, str, hash);
}

void lex_getline(struct lexer *lex)
{
top:
    cpp_read_line(&lex->cpp);
    if (lex->cpp.line_buf){
        cpp_process_line(&lex->cpp);
        if (lex->cpp.line_buf){
            lex->pch = lex->cpp.line_buf;
            if (!*lex->pch){
                // blank line
                goto top;
            }
        } else {
            lex->pch = NULL;
        }
    } else {
        lex->pch = NULL;
    }
}

void lex_start(struct lexer *lex)
{
    lex_getline(lex);
    lex_next(lex);
}

void lex_white(struct lexer *lex)
{
    lex->pch += strspn(lex->pch, " \t\n");
}

void lex_unget_tok(struct lexer *lex, struct token *token)
{
    free(lex->next_tok.tok_str);
    lex->next_tok = lex->tok;
    lex->tok = *token;
}

void lex_next(struct lexer *lex)
{
    if (lex->next_tok.tok != 0){
        // token stored with lex_unget_tok
        free(lex->tok.tok_str);
        lex->tok = lex->next_tok;
        memset(&lex->next_tok, 0, sizeof lex->next_tok);
        return;
    }

    if (!lex->pch || !*lex->pch){
        while (!lex->pch || !*lex->pch){
            lex_getline(lex);
            if (lex->pch){
                lex_white(lex);
            } else {
                // end of file
                lex->tok.tok = 0;
                lex->tok.tok_sloc = lex->cpp.line_loc;
                return;
            }
        }
    } else {
        lex_white(lex);
    }
    lex->tok.tok_sloc = lex->cpp.line_loc;
    if (isalpha(lex->pch[0]) || lex->pch[0] == '_'){
        // identifier or keyword
        char *p_start = lex->pch, *id_str = NULL;
        tok_t tok;
        struct ident *ident;
        while (isalnum(lex->pch[0]) || lex->pch[0] == '_'){
            lex->pch++;
        }
        strdncpy(&id_str, p_start, lex->pch - p_start);
        tok = find_keyword(id_str);
        if (tok != 0){
            lex->tok.tok = tok;
        } else {
            // identifier
            ident = lex_get_ident(lex, id_str);
            lex->tok.tok = ident->tok;
        }
        free(id_str);
    } else if (lex->pch[0] == '"' || lex->pch[0] == '\''){
        // string or character literal
        char quote = lex->pch[0], **str_data = &lex->tok.tok_str;
        int *pstr_data_len = &lex->tok.tok_str_len;
        lex->pch++;
        while (lex->pch[0] && lex->pch[0] != quote){
            strldcatc(str_data, pstr_data_len, lex->pch[0]);
            lex->pch++;
        }
        if (lex->pch[0] == quote){
            lex->pch++;
        } else {
            lex_error(lex, "unterminated string literal");
        }
        if (quote == '"'){
            lex->tok.tok = TOK_STR;
        } else {
            lex->tok.tok = TOK_CHARSTR;
        }
    } else if (lex->pch[0] >= '0' && lex->pch[0] <= '9'){
        // numeric constant
        char **num_str = &lex->tok.tok_str;
        // we can use cpp_lex_number - it does what we want :)
        cpp_lex_number(NULL, &lex->pch, num_str);
        lex->tok.tok = TOK_NUMBER;
    } else {
        // scan punctuation table
        // HOT code! optimize!
        int i, longest_match = 0, longest_match_len = 0, pch_len = strlen(lex->pch), punct_len;
        for (i=TOK_FIRST_PUNCT+1; i<TOK_INVAL; i++){
            punct_len = strlen(punctuation[i - (TOK_FIRST_PUNCT + 1)]);
            if (punct_len > pch_len || punct_len < longest_match_len){
                continue;
            }
            if (!strncmp(lex->pch, punctuation[i - (TOK_FIRST_PUNCT + 1)], punct_len)){
                assert(punct_len > longest_match_len);
                longest_match = i;
                longest_match_len = punct_len;
            }
        }
        if (longest_match){
            lex->pch += longest_match_len;
            lex->tok.tok = longest_match;
        } else     // single-character token?
        if (strchr("><=!-&|+*/%^.;:~(){}[],", lex->pch[0])){
            lex->tok.tok = lex->pch[0];
            lex->pch++;
        } else {
            lex_error(lex, "invalid character in input file: %c", lex->pch[0]);
            lex->tok.tok = 0;
        }
    }
}

// return a string for a token
// 'tok_str' may be null, but you won't get the contents of
// strings. The return value is a static string. Don't call lex_get_tok_str
// or lex_delete etc. until you've finished with the return value!
char *lex_get_tok_str(struct lexer *lex, tok_t tok, char *tok_str)
{
    static char buf[3];
    if (tok == 0){
        return "<no-token>";
    } else if (tok <= 255){
        sprintf(buf, "%c", tok);
        return buf;
    } else if (tok > TOK_FIRSTK && tok < TOK_LASTK){
        return keywords[tok - (TOK_FIRSTK + 1)];
    } else if (tok > TOK_FIRST_PUNCT && tok < TOK_LAST_PUNCT){
        return punctuation[tok - (TOK_FIRST_PUNCT + 1)];
    } else if (tok >= TOK_IDENT){
        // this is difficult, because they're all in a hash table
        // thankfully, we won't have to do this much
        int i;
        for (i=0; i<IDENT_HASH_SIZE; i++){
            struct ident *ident;
            ident = lex->ident_hashtab[i];
            while (ident && ident->tok != tok){
                ident = ident->hash_prev;
            }
            if (ident){
                return ident->str;
            }
        }
        return NULL;
    } else { // TODO: strings and punctuation-like tokens
        return NULL;
    }
}

bool lex_is_ident(struct lexer *lex, tok_t tok)
{
    return (tok >= TOK_IDENT && tok < lex->next_ident_tok);
}
