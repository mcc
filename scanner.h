#ifndef MCC_SCANNER_H
#define MCC_SCANNER_H

#include "cpp.h"

typedef int tok_t;

#define IDENT_HASH_SIZE 64

struct ident {
    struct ident *hash_prev;
    tok_t tok;
    char str[1];
};

struct token {
    tok_t tok;
    char *tok_str;
    int tok_str_len;
    struct sloc tok_sloc;
};

struct lexer {
    struct cpp cpp;
    char *pch;
    struct token tok;
    struct token next_tok; // tok pushed with lex_unget_tok
    int next_ident_tok;
    struct ident *ident_hashtab[IDENT_HASH_SIZE];
};

void token_dup(struct token *src, struct token *dest);
void token_free(struct token *token);

void lex_create(struct lexer *lex);
void lex_delete(struct lexer *lex);
void lex_next(struct lexer *lex);
void lex_unget_tok(struct lexer *lex, struct token *token);
char *lex_get_tok_str(struct lexer *lex, tok_t tok, char *tok_str);
void lex_start(struct lexer *lex);
bool lex_is_ident(struct lexer *lex, tok_t tok);

// Token numbers
#include "tokens.inc"
enum {
    TOK_FIRSTK = 0x200,
#define DEF(x) TOK_ ## x,
#include "tokens.inc"
#undef DEF
    TOK_LASTK,
};
enum {
    TOK_FIRST_PUNCT = 0x100,
#define PUNCT(x, str) TOK_ ## x,
#include "tokens.inc"
#undef PUNCT
    TOK_LAST_PUNCT,
};

#endif
