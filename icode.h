#ifndef MCC_ICODE_H
#define MCC_ICODE_H

#include "stree.h"

typedef enum {
    IC_NOP = 0,
    IC_ADD,
    IC_SUB,
    IC_MUL,
    // WARNING: add entries to icop_strs when adding entries here
} icopn_t;

struct icop {
    struct icop *prev, *next;
    int id; // debugging id
    icopn_t op;
    struct icop *operands[2];
};

struct icblock {
    int id; // debugging id
    struct icop *o_first, *o_last;
    struct icop *condition;  // condition for flow to next[0] or next[1]
    struct icblock *exit[2]; // next blocks in flow (0=condition is false, 1=condition is true)
};

void icblock_create(struct icblock *bl);
void icblock_destroy(struct icblock *bl);
void icblock_dump(struct icblock *bl);
struct icop *icblock_append(struct icblock *bl);

#endif
