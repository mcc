#ifndef MCC_ERRORS_H
#define MCC_ERRORS_H

#include "cppdefs.h"
#include "stddef.h"

#define die(...) _die(__func__, __VA_ARGS__)
noreturn void _die(const char *func, const char *fmt, ...);
void *emalloc(size_t size);
char *estrdup(const char *str);

#endif

