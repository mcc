#include "stree.h"
#include "scanner.h"
#include "cc.h"
#include "errors.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "assert.h"
#include "bstr.h"

static const char *form_strtab[] = {
    "NONE",
    "BINOP",
    "UNOP",
    "FACTOR",
    "TAG",
    "ATOM",
    "TYPE",
    "FUNCTION",
    "VARIABLE",
    "PARAMETERS",
    "BLOCK",
    "STAT",
    "PASS",
    "LABEL",
    "JUMP",
};

static const char *storage_class_strtab[] = {
    "",
    " static",
    " extern",
    " common",
};

static int next_id = 1;

const char *stree_form_str(int form)
{
    return form_strtab[form - STF_NONE];
}

const char *stree_storage_class_str(enum storage_class sc)
{
    return storage_class_strtab[sc];
}

struct stree *stree_create(void)
{
    struct stree *st;
    st = emalloc(sizeof(struct stree));
    memset(st, 0, sizeof (struct stree));
    st->id = next_id++;
    return st;
}

void stree_append_child(struct stree *st_parent, struct stree *st_child)
{
    struct stree *st_child_prev = st_parent->child;
    if (!st_parent || !st_child)
        return;
    st_child->parent = st_parent;
    if (st_child_prev){
        // insert at end of child list
        while (st_child_prev->next){
            st_child_prev = st_child_prev->next;
        }
        st_child_prev->next = st_child;
        st_child->prev = st_child_prev;
        st_child->next = NULL;
    } else {
        // first child
        st_parent->child = st_child;
        st_child->prev = NULL;
        st_child->next = NULL;
    }
}

void stree_remove_child(struct stree *st_child)
{
    struct stree *st_parent = st_child->parent;
    if (st_child->prev){
        st_child->prev->next = st_child->next;
    } else {
        st_parent->child = st_child->next;
    }
    if (st_child->next){
        st_child->next->prev = st_child->prev;
    }
    st_child->prev = NULL;
    st_child->next = NULL;
    st_child->parent = NULL;
}

void stree_destroy(struct stree *st)
{
    if (st){
        // destroy children
        struct stree *st_child = st->child,
          *st_child_next;

        while (st_child){
            st_child_next = st_child->next;
            stree_destroy(st_child);
            st_child = st_child_next;
        }
        
        if (st->cv.type == CBT_STRING){
            free(st->cv.v.string);
        }
        free(st);
    }
}

void stree_next_child(struct stree *st, struct stree **pchild)
{
    if (*pchild == NULL){
        *pchild = st->child;
    } else {
        *pchild = (*pchild)->next;
    }
}

// form a string representing 'node', for debug output
static char *make_type_str(struct stree *node)
{
    char *s = NULL;
    // is the type info valid?
    switch (node->form){
        case STF_VARIABLE:
        case STF_FUNCTION:
        case STF_TYPE:
            // XXX: erm, use arrays perhaps?
            if (node->btype.qualifiers & TQ_RESTRICT)
                strdcat(&s, "restrict ");
            if (node->btype.qualifiers & TQ_VOLATILE)
                strdcat(&s, "volatile ");
            if (node->btype.qualifiers & TQ_CONST)
                strdcat(&s, "const ");
            if (node->btype.qualifiers & TQ_SIGNED)
                strdcat(&s, "signed ");
            if (node->btype.qualifiers & TQ_UNSIGNED)
                strdcat(&s, "unsigned ");
            if (node->btype.qualifiers & TQ_LONG_LONG)
                strdcat(&s, "long long ");
            if (node->btype.qualifiers & TQ_LONG)
                strdcat(&s, "long ");
            if (node->btype.qualifiers & TQ_SHORT)
                strdcat(&s, "short ");
            switch (node->btype.base_type){
                case BT_VOID:
                    strdcat(&s, "void");
                    break;
                case BT_CHAR:
                    strdcat(&s, "char");
                    break;
                case BT_INT:
                    strdcat(&s, "int");
                    break;
                case BT_FLOAT:
                    strdcat(&s, "float");
                    break;
                case BT_DOUBLE:
                    strdcat(&s, "double");
                    break;
                case BT_POINTER:
                    strdcat(&s, "pointer");
                    // TODO: pointing to what?
                    break;
                case BT_STRUCT:
                    strdcat(&s, "struct");
                    // TODO: more informations!
                    break;
            }
            break;
        default:
            break;
    }
    return s;
}

static void stree_dump_internal(struct cc *cc, struct stree_stack *stack, FILE *output)
{
    struct stree_stack stack_item;
    {
        // dump this node
        struct stree_stack *p = stack;
        assert(p);
        // locate bottom of stack
        while (p->prev){
            p = p->prev;
        }
        while (p && p->next){
            if (p->st_node->next){
                fprintf(output, " |  ");
            } else {
                fprintf(output, "    ");
            }
            p = p->next;
        }
        if (stack->st_node->next){
            fprintf(output, " +--");
        } else {
            fprintf(output, " L--");
        }
        {
            char *type_str = make_type_str(stack->st_node);
            fprintf(output, "%d:%s [%s]%s%s%s\n",
                stack->st_node->id,
                stree_form_str(stack->st_node->form),
                lex_get_tok_str(&cc->lex, stack->st_node->tok, NULL),
                stree_storage_class_str(stack->st_node->storage_class),
                type_str ? " : " : "",
                type_str ? type_str : "");
            free(type_str);
        }
    }
    // and the children
    stack_item.prev = stack;
    stack_item.next = NULL;
    stack->next = &stack_item;
    stack_item.st_node = stack->st_node->child;
    while (stack_item.st_node){
        stree_dump_internal(cc, &stack_item, output);
        stack_item.st_node = stack_item.st_node->next;
    }
    stack->next = NULL; // pop stack
}
    

void stree_dump(struct cc *cc, struct stree *st_root, FILE *output)
{
    struct stree_stack stack_item;
    if (!st_root){
        fprintf(output, "[TREE:NULL]\n");
        return;
    }
    stack_item.st_node = st_root;
    stack_item.prev = NULL;
    stack_item.next = NULL;
    stree_dump_internal(cc, &stack_item, output);
}

struct stree *stree_right(struct stree *st)
{
    struct stree *p = st->child;
    while (p && p->next){
        p = p->next;
    }
    return p;
}

int stree_child_count(struct stree *st)
{
    int n = 0;
    struct stree *child_st;
    if (!st){
        return 0;
    }
    child_st = st->child;
    while (child_st){
        n++;
        child_st = child_st->next;
    }
    return n;
}

struct stree *stree_find_local(struct stree *scope, enum stree_form form, tok_t tok)
{
    if (!scope)
        return NULL;
    struct stree *p = scope->child;
    while (p && !(p->form == form && p->tok == tok)){
        p = p->next;
    }
    return p;
}

struct stree *stree_get_child_by_form(struct stree *st, enum stree_form form)
{
    struct stree *child_st;
    child_st = st->child;
    while (child_st && child_st->form != form){
        child_st = child_st->next;
    }
    return child_st;
}

