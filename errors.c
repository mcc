#define _BSD_SOURCE
#include "errors.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "stdarg.h"

noreturn void _die(const char *func, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    fprintf(stderr, "[%s] ", func);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fputc('\n', stderr);
    exit(1);
}

void *emalloc(size_t size)
{
    void *p = malloc(size);
    if (!p){
        die("memory full");
    }
    return p;
}

char *estrdup(const char *str)
{
    char *p;
    p = strdup(str);
    if (!p){
        die("memory full");
    }
    return p;
}

