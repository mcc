#ifndef MCC_SYMTAB_H
#define MCC_SYMTAB_H

#include "scanner.h"
#include "stdio.h"

// symbol type
enum symbol_type {
    ST_FUNC,  // a function
    ST_VAR,   // a variable
    ST_LABEL, // a label
};

// symbol scope
enum symbol_scope {
    SS_LOCAL,
    SS_LEXICAL, // lexical scope (ie. labels)
    SS_GLOBAL,
};

struct symbol {
    struct symbol *prev, *next;
    tok_t name; // name (as a token number)
    enum symbol_type type;
    enum symbol_scope scope;
};

struct symbol_table {
    struct symbol *first;
};

void symbol_table_create(struct symbol_table *st);
void symbol_table_free(struct symbol_table *st);
void symbol_table_dump(struct symbol_table *st, FILE *f);

// create and add a symbol
struct symbol *symbol_table_add(struct symbol_table *st, tok_t name);

// remove and free a symbol
void symbol_table_remove(struct symbol_table *st, struct symbol *sym);

#endif

