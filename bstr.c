#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "ctype.h"
#include "bstr.h"

char *strdup(const char *str)
{
    size_t len = strlen(str);
    char *new_str = malloc(len + 1);
    if (new_str){
        memcpy(new_str, str, len + 1);
    }
    return new_str;
}

char *strndup(const char *str, size_t n)
{
    size_t len = strnlen(str, n);
    char *new_str = malloc(len + 1);
    if (new_str){
        memcpy(new_str, str, len + 1);
    }
    return new_str;
}

char *strchrnul(const char *str, int c)
{
    const char *p = strchr(str, c);
    if (!p)
        p = (const char *) str + strlen(str);
    return (char *) p;
}

char *strdcat(char **dest, const char *src)
{
    int la, lb;
    char *p;
    la = *dest ? strlen(*dest) : 0;
    lb = strlen(src);
    p = realloc(*dest, la + lb + 1);
    if (!p)
        return NULL;
    *dest = p;
    memcpy(*dest + la, src, lb + 1);
    return *dest;
}

char *strdncat(char **dest, const char *src, size_t n)
{
    int la, lb;
    char *p;
    la = *dest ? strlen(*dest) : 0;
    lb = strlen(src);
    if (lb > n)
        lb = n;
    p = realloc(*dest, la + lb + 1);
    if (!p)
        return NULL;
    *dest = p;
    memcpy(*dest + la, src, lb);
    (*dest)[la + lb] = '\0';
    return *dest;
}

char *strdcpy(char **dest, const char *src)
{
    int l;
    char *p;
    l = strlen(src);
    p = realloc(*dest, l + 1);
    if (!p)
        return NULL;
    *dest = p;
    memcpy(*dest, src, l + 1);
    return *dest;
}

char *strdncpy(char **dest, const char *src, size_t n)
{
    int l;
    char *p;
    l = strlen(src);
    if (l > n)
        l = n;
    p = realloc(*dest, l + 1);
    if (!p)
        return NULL;
    *dest = p;
    memcpy(*dest, src, l);
    (*dest)[l] = '\0';
    return *dest;
}

char *strdcatc(char **dest, int ch)
{
    int l;
    char *p;
    l = *dest ? strlen(*dest) : 0;
    p = realloc(*dest, l + 2);
    if (!p)
        return NULL;
    *dest = p;
    (*dest)[l] = ch;
    (*dest)[l + 1] = '\0';
    return *dest;
}

char *strldcat(char **dest, int *dest_len, const char *src)
{
    int lb;
    char *p;
    lb = strlen(src);
    p = realloc(*dest, *dest_len + lb + 1);
    if (!p)
        return NULL;
    *dest = p;
    memcpy(*dest + *dest_len, src, lb + 1);
    *dest_len += lb;
    return *dest;
}

char *strldncat(char **dest, int *dest_len, const char *src, size_t n)
{
    int lb;
    char *p;
    lb = strlen(src);
    if (lb > n)
        lb = n;
    p = realloc(*dest, *dest_len + lb + 1);
    if (!p)
        return NULL;
    *dest = p;
    memcpy(*dest + *dest_len, src, lb);
    (*dest)[*dest_len + lb] = '\0';
    *dest_len += lb;
    return *dest;
}

char *strldcpy(char **dest, int *dest_len, const char *src, int src_len)
{
    char *p;
    p = realloc(*dest, src_len + 1);
    if (!p)
        return NULL;
    *dest = p;
    memcpy(*dest, src, src_len + 1);
    *dest_len = src_len;
    return *dest;
}

char *strldncpy(char **dest, int *dest_len, const char *src, size_t n)
{
    int l;
    char *p;
    l = strlen(src);
    if (l > n)
        l = n;
    p = realloc(*dest, l + 1);
    if (!p)
        return NULL;
    *dest = p;
    memcpy(*dest, src, l);
    (*dest)[l] = '\0';
    *dest_len = l;
    return *dest;
}

char *strldcatc(char **dest, int *dest_len, int ch)
{
    char *p;
    p = realloc(*dest, *dest_len + 2);
    if (!p)
        return NULL;
    *dest = p;
    (*dest)[*dest_len] = ch;
    (*dest)[*dest_len + 1] = '\0';
    (*dest_len)++;
    return *dest;
}

char *sdprintf(const char *fmt, ...)
{
    char *p;
    va_list ap;
    va_start(ap, fmt);
    p = vsdprintf(fmt, ap);
    va_end(ap);
    return p;
}

char *vsdprintf(const char *fmt, va_list ap)
{
    char buf[2], *p;
    int n;
    n = vsnprintf(buf, 2, fmt, ap);
    p = malloc(n + 1);
    if (!p)
        return NULL;
    vsprintf(p, fmt, ap);
    return p;
}

void strtolower(char *str)
{
    while (*str){
        *str = tolower(*str);
        str++;
    }
}

void strtoupper(char *str)
{
    while (*str){
        *str = toupper(*str);
        str++;
    }
}

char *pathname(const char *str)
{
    char *p = strrchr(str, '/');
    if (!p)
        return strdup(".");
    else
        return strndup(str, p - str);
}

char *filesuffix(const char *str)
{
    const char *p = str + strlen(str) - 1;
    while (p >= str){
        if (*p == '.'){
            // Suffix, or hidden file?
            if (p == str || p[-1] == '/'){
                // Hidden file. :-(
                return strdup("");
            } else {
                return strdup(p + 1);
            }
        } else
            p--;
    }
    return strdup("");
}

