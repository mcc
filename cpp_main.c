#include "cpp.h"

int cpp_main(int argc, char **argv)
{
    struct cpp s_cpp, *cpp = &s_cpp;
    const char *old_name = NULL;
    int old_linenum = 0;

    cpp_init(cpp);
    cpp_include_file(cpp, "<stdin>", stdin, false);
    do {
        cpp_read_line(cpp);
        if (cpp->line_buf){
            cpp_process_line(cpp);
            if (cpp->line_buf){
                if (old_name != cpp->line_loc.name
                || old_linenum != cpp->line_loc.line - 1){
                    old_name = cpp->line_loc.name;
                    old_linenum = cpp->line_loc.line;
                    printf("# %d \"%s\"\n", old_linenum, old_name);
                } else {
                    old_linenum++;
                }
                fputs(cpp->line_buf, stdout);
                fputc('\n', stdout);
            }
        }
    } while (cpp->include_top);
    cpp_delete(cpp);

    return 0;
}

